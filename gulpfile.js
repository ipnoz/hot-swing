
let gulp = require('gulp');
let gutil = require('gulp-util');
let del = require('del');
let browserSync = require('browser-sync').create();
let htmlExtender = require('gulp-html-extend');
let preservetime = require('gulp-preservetime');
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let uglify = require('gulp-uglify');
let rename = require("gulp-rename");
let imageResize = require('gulp-image-resize');

let SRC = './src';
let DEST = './dist';
let TMP = './tmp';

const paths = {
    css: {
        src: [SRC+'/scss/**/*.scss'],
        temp: TMP+'/*.css',
        dest: DEST+'/css'
    },
    js: {
        src: [SRC+'/js/**/*.js'],
        dest: DEST+'/js'
    },
    images: {
        src: [SRC+'/img/**/*'],
        dest: DEST+'/img'
    },
    html: {
        src: [SRC+'/*.html', SRC+'/*.php', '!'+SRC+'/master.php', '!'+SRC+'/*.inc.*'],
        dest: DEST+'/'
    },
    php: {
        src: [SRC+'/php/**/*.php', '!'+SRC+'/php/fixtures/*'],
        dest: DEST+'/php'
    },
    audio: {
        src: [SRC+'/audio/**/*'],
        dest: DEST+'/audio'
    },
    video: {
        src: [SRC+'/video/**/*'],
        dest: DEST+'/video'
    },
    datas: {
        src: [SRC+'/datas/**/*'],
        dest: DEST+'/datas'
    },
    posters: {
        src: [SRC+'/datas/posters/**/*'],
        dest: SRC+'/datas/posters-w400/'
    }
};

const HTTP_PATH = '/var/www/html';
const START_PATH = __dirname.replace(HTTP_PATH, '')+'/'+DEST;

/**
 * TASKS
 */

gulp.task('clean', function () {
    return del([DEST]);
});

// vendor
gulp.task('vendor', function() {
    gulp.src([SRC+'/vendor/*/*.min.*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/fonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/fonts'))
        .pipe(preservetime());
    return gulp.src([SRC+'/vendor/webfonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/webfonts'))
        .pipe(preservetime());
});

// html
gulp.task('html', function() {
    return gulp.src(paths.html.src)
        .pipe(htmlExtender({ annotations: false }))
        .pipe(gulp.dest(paths.html.dest))
        .pipe(browserSync.stream());
});

// css
function compileScss() {
    return gulp.src(paths.css.src)
        .pipe(sass.sync({outputStyle:'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest(TMP));
}

function MinifyCss() {
    return gulp.src(paths.css.temp)
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.css.dest))
        .pipe(browserSync.stream());
}

gulp.task('css', gulp.series(compileScss, MinifyCss));

// js
function compileJs() {
    return gulp.src(paths.js.src)
        .pipe(uglify().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.js.dest))
        .pipe(browserSync.stream());
}

gulp.task('js', gulp.series(compileJs));

// img
gulp.task('img', function() {
    return gulp.src(paths.images.src)
        .pipe(gulp.dest(paths.images.dest))
        .pipe(preservetime());
});

// audio
gulp.task('audio', function() {
    return gulp.src(paths.audio.src)
        .pipe(gulp.dest(paths.audio.dest))
        .pipe(preservetime());
});

// video
gulp.task('video', function() {
    return gulp.src(paths.video.src)
        .pipe(gulp.dest(paths.video.dest))
        .pipe(preservetime());
});

// php
gulp.task('php', function() {
    return gulp.src(paths.php.src)
        .pipe(gulp.dest(paths.php.dest))
        .pipe(preservetime())
        .pipe(browserSync.stream());
});

// datas
gulp.task('datas', function() {
    return gulp.src(paths.datas.src)
        .pipe(gulp.dest(paths.datas.dest))
        .pipe(preservetime())
        .pipe(browserSync.stream());
});

// Posters resizer
gulp.task('posters:resize', function () {
    return gulp.src(paths.posters.src)
        .pipe(imageResize({
            imageMagick: true,
            width : 400,
            crop : false,
            upscale : true,
            quality: 0.75
        }))
        .pipe(gulp.dest(paths.posters.dest));
});

function browserSyncDaemon() {
    browserSync.init({
        proxy: '172.18.0.1',
        open: false,
        notify: false,
        logFileChanges: false,
        reloadOnRestart: true,
        ghostMode: false,
        startPath: START_PATH,
    });
}

function watchFiles() {
    gulp.watch(SRC+'/*.html', gulp.series('html'));
    gulp.watch(SRC+'/*.php', gulp.series('html'));
    gulp.watch(paths.css.src, gulp.series('css'));
    gulp.watch(paths.js.src, gulp.series('js'));
    gulp.watch(paths.php.src, gulp.series('php'));
    gulp.watch(SRC+'/datas/**/*.txt', gulp.series('datas'));
}

gulp.task('default', gulp.parallel(browserSyncDaemon, watchFiles));
gulp.task('build',  gulp.series('vendor', 'html', 'css', 'js', 'img', 'audio', 'video', 'php', 'datas'));
gulp.task('rebuild', gulp.series('clean', 'build'));
