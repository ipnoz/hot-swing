<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;

use Sonata\CoreBundle\Validator\ErrorElement;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Sonata\Form\Type\DateTimePickerType;
use Sonata\Form\Type\BooleanType;

class EventAdmin extends AbstractAdmin
{
    /*
     * Pager: max lignes per pages
     * 0 for display one page.
     */
    protected $maxPerPage = 32;

    protected $datagridValues = [
        '_sort_by' => 'start'
    ];

    protected $dateFormat = 'd.m.Y';
    protected $dateTimeFormat = 'd.m.Y à H:i:s';

    /*
     * Add an FA icon to the label
     */
    protected $labelIcon = 'fa fa-envelope';

    /*
     * Return label icon string for the twig template
     */
    public function getLabelIcon(): string {
        return $this->labelIcon;
    }

    /*
     * Personifiate db query
     */
//    public function createQuery($context = 'list')
//    {
//        $query = parent::createQuery($context);
//        // Force to order enabled emails at the top of the grid.
//        $query->orderBy($query->getRootAliases()[0].'.isEnabled', 'DESC');
//        return $query;
//    }

    /*
    * Filters forms fields
    */
//    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
//    {
//        $datagridMapper
//            ->add('email')
//            ->add('comment')
//        ;
//    }

    /*
    * List fields
    */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('_action', null, [
                'label' => false,
                'actions' => [
                    'show' => [],
                ]
            ])
            ->addIdentifier('title')
            ->add('start', 'date', [
                'format' => 'd-m-Y'
            ])
            ->add('end', 'date', [
                'format' => 'd-m-Y'
            ])
//            ->add('isEnabled', 'boolean', [
//                'label' => false,
//                'label_icon' => 'fa fa-toggle-on',
//                'editable' => true,
//                'sortable' => false,
//            ])
        ;
    }

    /*
    * Create/edit forms fields
    */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('startDateTimeInterface', DateTimePickerType::class, [
                'dp_side_by_side' => true,
                'dp_use_seconds' => false,
                'dp_minute_stepping' => 5,
                'date_format' => 'dd-MM-yyyy\'T\'HH:mm',
            ])
            ->add('duration', IntegerType::class, [
                'attr' => ['min' => 1, 'max' => 12]
            ])
            ->add('title', TextType::class, ['required' => false])
            ->add('venue', TextType::class, ['required' => false])
            ->add('city', TextType::class, ['required' => false])
            ->add('country', TextType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('isFree', BooleanType::class, ['required' => false])
            ->add('cost', TextType::class, ['required' => false])
            ->add('ticketUrl', TextType::class, ['required' => false])
            ->add('iconPath', TextType::class, ['required' => false])
        ;
    }

    /*
    * Fields to be shown on show action
    */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Event')
            ->add('id')
            ->add('start', 'date', ['format' => $this->dateTimeFormat])
            ->add('end', 'date', ['format' => $this->dateTimeFormat])
            ->add('title')
            ->add('venue')
            ->add('city')
            ->add('country')
            ->add('address')
            ->add('isFree')
            ->add('cost')
            ->add('ticketUrl')
            ->add('iconPath')
        ;
    }

    /**
    * Add batch actions
    */
//    public function configureBatchActions($actions)
//    {
//        if (
//            $this->hasRoute('edit') && $this->hasAccess('edit')
//        ) {
//            $action['toggleStatus'] = [
//                'label' => 'action_toggle_status',
//                'ask_confirmation' => false,
//            ];
//            // Add toggleStatus on top of the actions array
//            $actions = array_merge($action, $actions);
//        }
//        return $actions;
//    }

    /**
     * Saving hooks, see doc:
     * https://sonata-project.org/bundles/admin/3-x/doc/reference/saving_hooks.html
     */
    public function preValidate($object)
    {
        $dt = $object->getStartDateTimeInterface();
        if (is_object($dt)) {
            $object->setStart($dt->getTimestamp());
            $object->setend($dt->getTimestamp() + ($object->getDuration()*3600));
            $object->setStartDateTimeFormat($dt->format('Y-m-d H:i:s'));
        } else {
            $dt = new \DateTime($object->getStartDateTimeFormat());
            $object->setStartDateTimeInterface($dt->format('Y-m-d H:i:s'));
        }
//        $timestamp = $object->getStart()->getTimestamp();
//        $object->setStart($timestamp);
//        $object->setend($timestamp + ($object->getDuration()*3600));
    }

//    function prePersist($object)
//    {
//        var_dump($object); die;
//        $object->setCreatedAt(time());
//        $object->setUpdatedAt(time());
//        $object->setIsEnabled(true);
//    }
//
//    public function preUpdate($object)
//    {
//        $object->setUpdatedAt(time());
//    }
}
