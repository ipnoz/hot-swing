<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $start
     *
     * @ORM\Column(type="integer")
     */
    private $start;

    /**
     * @var string $startDateTimeFormat
     *
     * @ORM\Column
     */
    private $startDateTimeFormat;

    /**
     * Non-persisted field for the start \DateTimeInterface object.
     *
     * @var string $startDateTimeInterface
     */
    private $startDateTimeInterface;

    /**
     * @var integer $end
     *
     * @ORM\Column(type="integer")
     */
    private $end;

    /**
     * @var integer $duration
     *
     * @ORM\Column
     */
    private $duration;

    /**
     * @var string $title
     *
     * @ORM\Column(nullable=true)
     */
    private $title;

    /**
     * @var string $venue
     *
     * @ORM\Column(nullable=true)
     */
    private $venue;

    /**
     * @var string $city
     *
     * @ORM\Column(nullable=true)
     */
    private $city;

    /**
     * @var string $country
     *
     * @ORM\Column(nullable=true)
     */
    private $country;

    /**
     * @var string $address
     *
     * @ORM\Column(nullable=true)
     */
    private $address;

    /**
     * @var boolean $isFree
     *
     * @ORM\Column(type="boolean")
     */
    private $isFree = true;

    /**
     * @var string $cost
     *
     * @ORM\Column(nullable=true)
     */
    private $cost;

    /**
     * @var string $ticketUrl
     *
     * @ORM\Column(nullable=true)
     */
    private $ticketUrl;

    /**
     * @var string $iconPath
     *
     * @ORM\Column(nullable=true)
     */
    private $iconPath;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return string
     */
    public function getStartDateTimeFormat()
    {
        return $this->startDateTimeFormat;
    }

    /**
     * @param string $startDateTimeFormat
     */
    public function setStartDateTimeFormat($startDateTimeFormat)
    {
        $this->startDateTimeFormat = $startDateTimeFormat;
    }

    /**
     * @return string
     */
    public function getStartDateTimeInterface()
    {
        return $this->startDateTimeInterface;
    }

    /**
     * @param string $startDateTimeInterface
     */
    public function setStartDateTimeInterface($startDateTimeInterface)
    {
        $this->startDateTimeInterface = $startDateTimeInterface;
    }



    /**
     * @return int
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param int $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }


    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * @param mixed $venue
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getisFree()
    {
        return $this->isFree;
    }

    /**
     * @param mixed $isFree
     */
    public function setIsFree($isFree)
    {
        $this->isFree = $isFree;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getTicketUrl()
    {
        return $this->ticketUrl;
    }

    /**
     * @param mixed $ticketUrl
     */
    public function setTicketUrl($ticketUrl)
    {
        $this->ticketUrl = $ticketUrl;
    }

    /**
     * @return mixed
     */
    public function getIconPath()
    {
        return $this->iconPath;
    }

    /**
     * @param mixed $iconPath
     */
    public function setIconPath($iconPath)
    {
        $this->iconPath = $iconPath;
    }
}


