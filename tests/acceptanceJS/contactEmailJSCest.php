<?php

use Page\ContactPage as Page;

/**
 * Class contactEmailJSCest
 *
 * Test to send a contact email with the jQuery AJAX function
 *
 * Run "bin/codecept run -x skip" to skip the skip group ^_^
 * @group skip
 * @group email
 */
class contactEmailJSCest
{
    public function send_contact_email_with_ajax_works(AcceptanceJSTester $I)
    {
        $I->amOnPage(Page::location);

        /**
         * Workaround: Remove the autoplayed video in contact page with jQuery
         * to decrease time of the test
         */
        $I->executeJS('$("#teaser-vid").remove();');

        $I->fillField(Page::fieldName, Page::testName);
        $I->fillField(Page::fieldEmail, Page::testEmail);
        $I->fillField(Page::fieldSubject, Page::testSubject.' (AJAX)');
        $I->fillField(Page::fieldMessage, Page::testMessage);
        $I->click(Page::submitButton);
        $I->waitForJS('return $.active == 0;', 30);
        $I->seeElement('.ajax-alert.alert-success');
    }
}
