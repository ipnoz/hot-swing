<?php

use Page\ContactPage as Page;

/**
 * Class contactEmailCest
 *
 * Testing some scenarios for the contact email form.
 * First test will try to send a valid email.
 * Then test the handler for incorrect datas submition.
 *
 * Run "bin/codecept run -x skip" to skip the skip group ^_^
 * @group skip
 * @group email
 */
class contactEmailCest
{
    public function send_contact_email_with_empty_name_field_must_fail(AcceptanceTester $I)
    {
        $I->amOnPage(Page::location);
        $I->fillField(Page::fieldName, '');
        $I->fillField(Page::fieldEmail, Page::testEmail);
        $I->fillField(Page::fieldSubject, Page::testSubject);
        $I->fillField(Page::fieldMessage, Page::testMessage);
        $I->click(Page::submitButton);
        $I->seeElement('.alert.alert-error');
    }

    public function send_contact_email_with_empty_email_field_must_fail(AcceptanceTester $I)
    {
        $I->amOnPage(Page::location);
        $I->fillField(Page::fieldName, Page::testName);
        $I->fillField(Page::fieldEmail, '');
        $I->fillField(Page::fieldSubject, Page::testSubject);
        $I->fillField(Page::fieldMessage, Page::testMessage);
        $I->click(Page::submitButton);
        $I->seeElement('.alert.alert-error');
    }

    public function send_contact_email_with_empty_message_field_must_fail(AcceptanceTester $I)
    {
        $I->amOnPage(Page::location);
        $I->fillField(Page::fieldName, Page::testName);
        $I->fillField(Page::fieldEmail, Page::testEmail);
        $I->fillField(Page::fieldSubject, Page::testSubject);
        $I->fillField(Page::fieldMessage, '');
        $I->click(Page::submitButton);
        $I->seeElement('.alert.alert-error');
    }

    public function send_contact_email_with_empty_subject_field_must_works(AcceptanceTester $I)
    {
        $I->amOnPage(Page::location);
        $I->fillField(Page::fieldName, Page::testName);
        $I->fillField(Page::fieldEmail, Page::testEmail);
        $I->fillField(Page::fieldSubject, '');
        $I->fillField(Page::fieldMessage, Page::testMessage);
        $I->click(Page::submitButton);
        $I->seeElement('.alert.alert-success');
    }

    public function send_contact_email_with_all_field_must_works(AcceptanceTester $I)
    {
        $I->amOnPage(Page::location);
        $I->fillField(Page::fieldName, Page::testName);
        $I->fillField(Page::fieldEmail, Page::testEmail);
        $I->fillField(Page::fieldSubject, Page::testSubject);
        $I->fillField(Page::fieldMessage, Page::testMessage);
        $I->click(Page::submitButton);
        $I->seeElement('.alert.alert-success');
    }
}
