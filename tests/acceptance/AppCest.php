<?php

/**
 * Class AppCest
 *
 * Check all HTML pages to determine if that App.php didn't crash badly.
 * But it didn't means that's App.php didn't produced some errors during the
 * process.
 */
class AppCest
{
    /**
     * The web page is complete if we can see the last footer element.
     *
     * @dataProvider uri_Provider
     */
    public function front_page_works(
        AcceptanceTester $I, \Codeception\Example $exemple)
    {
        $I->wantToTest($exemple['uri']);
        $I->amOnPage($exemple['uri']);
        $I->see('David Dadon','//body//footer');
    }

    /**
     * Musiciens datas provider function.
     *
     * @return array
     */
    private function uri_Provider()
    {
        return [
            'Index page' => ['uri' => '/'],
            'Concerts page' => ['uri' => '/concerts-tour.php'],
            'Past concerts page' => ['uri' => '/concerts-past.php'],
            'Albums page' => ['uri' => '/albums.php'],
            'Contact page' => ['uri' => '/contact.php'],
            'Mentions legales page' => ['uri' => '/mentions-legales.php'],
            'Pro page' => ['uri' => '/pro.php'],
            'Pro dossier presse page' => ['uri' => '/pro-dossier-presse.php'],
            'Pro fiche technique page' => ['uri' => '/pro-fiche-technique.php'],
        ];
    }
}
