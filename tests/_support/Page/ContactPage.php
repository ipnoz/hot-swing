<?php

namespace Page;

/**
 * Class ContactPage
 *
 * Enum HTML locator for the contact page.
 */
class ContactPage
{
    /**
     * Contact page URN location
     */
    const location = '/contact.php';

    /**
     * The contat form HTML element locator
     */
    const formEl = 'form#contact';

    /**
     * Contact fields names
     */
    const fieldName = 'name';
    const fieldEmail = 'email';
    const fieldSubject = 'subject';
    const fieldMessage = 'message';

    /**
     * Contact submit form button locator
     */
    const submitButton = self::formEl.' button';

    /**
     * Datas to populate tests emails
     */
    const testName = 'testName';
    const testEmail = 'test@test.tld';
    const testSubject = 'testSubject';
    const testMessage =
        'Test message: hello tester ! <br/> <strong>Test HTML strip tags in message</strong>
        Test UTF8:
        <i>test French accent in message àéèùç </i>
        <div>test other $^%*µ!§# < / \ > </div>';
}
