#!/bin/bash

source "./.env" || exit 2

FTPURL="ftp://$APP_DEPLOY_USER:$APP_DEPLOY_PASS@$APP_DEPLOY_HOST"

DIR_LOCAL="./dist/"
DIR_REMOTE="/www/"

OPTIONS="--reverse --verbose --delete --overwrite --only-newer --no-perms --exclude datas/"

# ##########
#
# PROCESS
#
# ##########


cd "$(dirname "$0")"

# Setup debug mode
if [ $APP_DEPLOY_DEBUG != 0 ]; then
	OPTIONS="$OPTIONS --dry-run"
	echo ""
    echo "mode debug activé"
    echo
fi

lftp -c "set ftp:list-options -a;
lftp -c set cmd:fail-exit yes;

open '$FTPURL';
lcd $DIR_LOCAL;
cd $DIR_REMOTE;
mirror $OPTIONS"
