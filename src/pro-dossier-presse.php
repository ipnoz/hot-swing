<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area bg-overlay2"></div>

    <div class="bg-gradients"></div>

    <section id="dossier-presse-section">

        <div class="container">

            <div class="breadcrumb">
                <a href="pro.php<?= $LANG->getUrlParam(); ?>">/<?= $LANG->L('breadcrumb_pro_page');?></a>/<?= $LANG->L('breadcrumb_press_page'), PHP_EOL;?>
            </div>

            <div class="pages">

                <div class="downloadBtn">
                    <a class="btn musica-btn" href="datas/pro/dossier-de-presse.pdf" download="dossier-de-presse-Hot-Swing-Sextet.pdf">
                        <?= $LANG->L('download_press_file'), PHP_EOL;?>
                    </a>
                </div>

                <div class="page">
                    <img src="img/pro/dossier-presse/page-000.jpg" alt="page 1"/>
                    page 1
                </div>
                <div class="bg-gradients-inverse"></div>

                <div class="downloadBtn">
                    <a class="btn musica-btn" href="datas/pro/dossier-de-presse.pdf" download="dossier-de-presse-Hot-Swing-Sextet.pdf">
                        <?= $LANG->L('download_press_file'), PHP_EOL;?>
                    </a>
                </div>

            </div>
        </div>

    </section>
    <div class="bg-gradients"></div>

    <!-- @@include cta-section.inc.html {"css":"pro"} -->
<!-- @@close -->
