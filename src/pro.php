<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area bg-overlay2"></div>

    <div class="bg-gradients"></div>

    <section id="pro-section">

        <div class="container">

            <div class="visuels-area">

                <div class="links-area">
                    <h6 class="wow fadeInLeft">
                        <?= $LANG->L('pro_general_info'), PHP_EOL;?>
                    </h6>
                    <a class="btn musica-btn btn-3 wow fadeInLeft" href="pro-dossier-presse.php<?= $LANG->getUrlParam(); ?>">
                        <?= $LANG->L('see_press_page'), PHP_EOL;?>
                    </a>
                    <a class="btn musica-btn btn-3 wow fadeInRight" href="pro-fiche-technique.php<?= $LANG->getUrlParam(); ?>">
                        <?= $LANG->L('see_technical_page'), PHP_EOL;?>
                    </a>
                </div>

                <p class="desc-1 wow fadeInLeft">
                    <?= $LANG->L('HD_photo_to_print'); ?><br/>
                    <?= $LANG->L('clic_for_download'); ?><br/>
                    <?= sprintf($LANG->L('size_and_weight'), '2014 x 1547', '1,40 Mo'), PHP_EOL;?>
                </p>

                <a class="poster poster-1 wow fadeInRight" href="img/pro/visuel-sans-titre-original-2014x1547.jpg" download="Visuel-Hot-Swing-Sextet.jpg">
                    <img src="img/pro/visuel-sans-titre.jpg" alt=""/>
                </a>

                <a class="poster poster-2 wow fadeInLeft" href="img/pro/poster-original-5031x7087.jpeg" download="poster-Hot-Swing-Sextet.jpeg">
                    <img src="img/pro/poster.jpeg" alt=""/>
                </a>

                <p class="desc-2 wow fadeInRight">
                    <?= $LANG->L('HD_photo_to_print'); ?><br/>
                    <?= $LANG->L('clic_for_download'); ?><br/>
                    <?= sprintf($LANG->L('size_and_weight'), '5031 x 7087', '2,30 Mo'), PHP_EOL;?>
                </p>
            </div>

            <h5><?= $LANG->L('references_scenes'); ?></h5>

<?php require PATH_APP.'php/View/Scenes.php'; ?>

        </div>
    </section>

    <div class="bg-gradients-inverse"></div>

    <!-- @@include cta-section.inc.html {"css":"pro"} -->
<!-- @@close -->
