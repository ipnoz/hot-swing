<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area"></div>

    <div class="bg-gradients"></div>

    <section id="albums-section">

        <div class="container">
            <div class="album whats-your-give">

                <div class="cover-area">
                    <img src="img/album/mock-up-whats-your-jive.png" alt=""/>
                </div>

                <div class="description-area">

                    <div class="date"><?= $LANG->L('month_12'); ?> 2020</div>
                    <h4>What's your jive ?</h4>
                    <div class="my-3">
                        <strong>
                            <i class="fab fa-product-hunt"></i>
                            <i class="fas fa-copyright"></i>
                            Mélodinote – InOuïe Distribution
                            <img class="melodinote" src="img/vendor/melodinote/logo-noir-64x64.png" alt=""/>
                        </strong>
                    </div>
                    <ul class="key-notes">
                        <li><i class="fas fa-music"></i>Thibaud Bonté : trompette</li>
                        <li><i class="fas fa-music"></i>Bertrand Tessier : saxophone ténor</li>
                        <li><i class="fas fa-guitar"></i>Erwann Muller : guitare</li>
                        <li><i class="fas fa-guitar"></i>Ludovic Langlade : guitare</li>
                        <li><i class="fas fa-music"></i>Franck Richard : contrebasse</li>
                        <li><i class="fas fa-drum"></i>Jéricho Ballan : batterie</li>
                    </ul>
                    <ul class="key-notes">
                        <li><i class="fas fa-headphones"></i> Enregistrement : It is my loft - Théâtre - La Caillère-Saint-Hilaire (85)</li>
                        <li><i class="fas fa-microphone"></i>Prise de son & mixage : François Baudoin - All mëe productions</li>
                        <li><i class="fas fa-headset"></i>Mastering : Alexis Bardinet – Globe Audio</li>
                        <li><i class="fas fa-pencil-alt"></i>Illustration et graphisme : Fabien Doulut</li>
                    </ul>

                    <div class="buy-buttons">
                        <a href="https://www.paniermusique.fr/les-cds/4678-what-s-your-jive-hot-swing-sextet-3760301213826.html" class="btn musica-btn" title="Inouïe distribution">
                            <?= $LANG->L('buy_on'); ?> Inouïe
                        </a>
                        <a href="https://melodinoteproduction.bandcamp.com/album/whats-your-jive" class="btn musica-btn" title="Bandcamp">
                            <?= $LANG->L('buy_on'); ?> Bandcamp
                        </a>
                        <a href="https://open.spotify.com/album/0mYC5A32mK4xrfGXo0IyBa" class="btn musica-btn" title="Spotify">
                            <?= $LANG->L('buy_on'); ?> <i class="fab fa-spotify" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.deezer.com/fr/album/188844442" class="btn musica-btn" title="Deezer">
                            <?= $LANG->L('buy_on'); ?> Deezer
                        </a>
                        <a href="https://music.apple.com/fr/album/whats-your-jive/1542018686?uo=4&at=1l3v9Tx&ls=1&app=music" class="btn musica-btn" title="Apple">
                            <?= $LANG->L('buy_on'); ?> <i class="fab fa-apple" aria-hidden="true"></i>
                        </a>
                    </div>

                </div>

                <div class="scene-area">
                    <img src="img/album/livret-whats-your-jive.jpg" alt=""/>
                </div>
            </div>

            <div class="album black-market-stuff">
                <div class="cover-area">
                    <img src="img/album/mock-up-black-market-stuff.png" alt=""/>
                </div>

                <div class="description-area">

                    <div class="date"><?= $LANG->L('month_09'); ?> 2018</div>
                    <h4>Black Market Stuff</h4>
                    <div class="my-3">
                        <strong>
                            <i class="fab fa-product-hunt"></i>
                            <i class="fas fa-copyright"></i>
                            Mélodinote – InOuïe Distribution
                            <img class="melodinote" src="img/vendor/melodinote/logo-noir-64x64.png" alt=""/>
                        </strong>
                    </div>
                    <ul class="key-notes">
                        <li><i class="fas fa-music"></i>Thibaud Bonté : trompette</li>
                        <li><i class="fas fa-music"></i>Bertrand Tessier : saxophone ténor, clarinette</li>
                        <li><i class="fas fa-guitar"></i>Erwann Muller : guitare</li>
                        <li><i class="fas fa-guitar"></i>Ludovic Langlade : guitare</li>
                        <li><i class="fas fa-music"></i>Franck Richard : contrebasse</li>
                        <li><i class="fas fa-drum"></i>Jéricho Ballan : batterie</li>
                    </ul>
                    <ul class="key-notes">
                        <li><i class="fas fa-music"></i>Jérôme Gatius : clarinette sur #8</li>
                    </ul>
                    <ul class="key-notes">
                        <li><i class="fas fa-headphones"></i>Enregistrement : L’Antirouille – Rock & Chanson</li>
                        <li><i class="fas fa-microphone"></i>Prise de son : Sébastien Vaillier</li>
                        <li><i class="fas fa-headphones-alt"></i>Mixage : Didier Ottaviani</li>
                        <li><i class="fas fa-headset"></i>Mastering : Alexis Bardinet – Globe Audio</li>
                        <li><i class="fas fa-pencil-alt"></i>Illustration et graphisme : Fabien Doulut</li>
                    </ul>

                    <div class="buy-buttons">
                        <a href="https://www.paniermusique.fr/les-cds/2385-hot-swing-sextet-black-market-stuff-3760231765617.html" class="btn musica-btn" title="Inouïe distribution">
                            <?= $LANG->L('buy_on'); ?> Inouïe
                        </a>
                        <a href="https://melodinoteproduction.bandcamp.com/album/black-market-stuff" class="btn musica-btn" title="Bandcamp">
                            <?= $LANG->L('buy_on'); ?> Bandcamp
                        </a>
                        <a href="https://open.spotify.com/artist/0FywyaLz7YX2ejwWLM0MSq" class="btn musica-btn" title="Spotify">
                            <?= $LANG->L('buy_on'); ?> <i class="fab fa-spotify" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.deezer.com/fr/artist/8906214" class="btn musica-btn" title="Deezer">
                            <?= $LANG->L('buy_on'); ?> Deezer
                        </a>
                        <a href="https://music.apple.com/fr/album/black-market-stuff/1418704083" class="btn musica-btn" title="Itunes">
                            <?= $LANG->L('buy_on'); ?> <i class="fab fa-apple" aria-hidden="true"></i>
                        </a>
                    </div>

                </div>

                <div class="scene-area">
                    <img src="img/bg/7964-wk-01-1080.jpg" alt=""/>
                </div>
            </div>

            <div class="album hot-swing-sextet">

                <div class="cover-area">
                    <img src="img/album/mock-up-hot-swing-sextet.png" alt=""/>
                </div>

                <div class="description-area">
                    <div class="date"><?= $LANG->L('month_09'); ?> 2015</div>
                    <h4>Hot Swing Sextet</h4>
                    <div class="my-3">
                        <strong>
                            <i class="fab fa-product-hunt"></i>
                            <i class="fas fa-copyright"></i>
                            Mélodinote
                            <img class="melodinote" src="img/vendor/melodinote/logo-noir-64x64.png" alt=""/>
                        </strong>
                    </div>
                    <ul class="key-notes">
                        <li><i class="fas fa-music"></i>Thibaud Bonté : trompette</li>
                        <li><i class="fas fa-music"></i>Jérôme Gatius : clarinette</li>
                        <li><i class="fas fa-guitar"></i>Erwann Muller : guitare</li>
                        <li><i class="fas fa-guitar"></i>Ludovic Langlade : guitare</li>
                        <li><i class="fas fa-music"></i>Franck Richard : contrebasse</li>
                        <li><i class="fas fa-drum"></i>Jéricho Ballan : batterie</li>
                    </ul>
                    <ul class="key-notes">
                        <li><i class="fas fa-headphones"></i>Enregistrement et mixage : Sébastien Vaillier</li>
                        <li><i class="fas fa-headset"></i>Mastering : Didier Ottaviani</li>
                        <li><i class="fas fa-camera"></i>Photo : Fred Encuentra</li>
                        <li><i class="fas fa-pencil-alt"></i>Graphisme : Kosept.com</li>
                    </ul>

                    <blockquote class="mb-4">
                        « On oublie trop que dans les années 20, ces <strong><i>années folles</i></strong>, les
                        <strong><i>roaring twenties</i></strong>, le jazz était une musique de danse, une musique
                        populaire. Le <strong>Hot Swing Sextet</strong> lui s’en souvient et il nous ressuscite le
                        swing dans ce dernier album, cette musique qui balance et ne peut vous laisser immobile. Des
                        standards absolus nous sont proposés sur lesquels le Hot Swing Sextet mêle le tonus de la
                        trompette et la clarté de la clarinette à la pompe manouche des guitares le tout sur une
                        rythmique impeccable. Du jazz à danser certes mais aussi à écouter grâce aux arrangements
                        originaux et à l’interprétation parfaite. »
                    </blockquote>
                    <div>
                        Philippe Desmond – <strong>Action Jazz</strong>
                    </div>

                    <div class="buy-buttons">
                        <a href="https://melodinoteproduction.bandcamp.com/album/hot-swing-sextet" class="btn musica-btn" title="Bandcamp">
                            <?= $LANG->L('buy_on'); ?> Bandcamp
                        </a>
                        <a href="https://www.amazon.fr/Hot-Swing-Sextet/dp/B015LC6E4M/ref=sr_1_2?s=dmusic&ie=UTF8&qid=1549103023&sr=1-2-mp3-albums-bar-strip-0&keywords=Hot+Swing+Sextet" class="btn musica-btn" title="Amazon">
                            <?= $LANG->L('buy_on'); ?> <i class="fab fa-amazon" aria-hidden="true"></i>
                        </a>
                        <a href="https://itunes.apple.com/fr/album/hot-swing-sextet/1041190504" class="btn musica-btn" title="Itunes">
                            <?= $LANG->L('buy_on'); ?> <i class="fab fa-apple" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <div class="scene-area">
                    <img src="img/musiciens/Erwann-bg.jpg" alt=""/>
                </div>
            </div>
        </div>
    </section>

    <div class="bg-gradients-inverse"></div>

    <!-- @@include cta-section.inc.html {"css":"albums bg-overlay2"} -->
<!-- @@close -->
