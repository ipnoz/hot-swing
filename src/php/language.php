<?php

/**
 * This class is for toggling 2 languages only.
 * Hot swing doesn't need more, why bother, huh ?
 * But I'm sorry for the next dev who will have to rewrite this for add a third
 * one ^_^
 */
class language
{
    private $locales_path;

    private $default;
    private $current;

    private $locales;

    public function __construct($locales_path, $default)
    {
        $this->locales_path = $locales_path;

        $this->default = $default;
        $this->current = $default;

        // Change current language if requested by user
        if (isset($_GET['lang'])) {
            if ($_GET['lang'] === 'en') {
                $this->current = 'en';
            }
        }

        $LANG = [];

        require_once $this->locales_path.$this->default.'.php';

        if ($this->current !== $this->default) {
            require_once $this->locales_path.$this->current.'.php';
        }

        $this->locales = $LANG;
    }

    /**
     * Return the value of the locales key.
     */
    public function L($key)
    {
        return $this->locales[$key];
    }

    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Return language Url parameter
     */
    function getUrlParam() {
        if ($this->current !== $this->default) {
            return '?lang='.$this->current;
        }
    }

    public function getMenuParam()
    {
        if ($this->current === 'fr') {
            $lang = 'en';
        } else {
            $lang = 'fr';
        }
        return $lang;
    }

    public function getMenuText()
    {
        if ($this->current === 'fr') {
            $text = 'English';
        } else {
            $text = 'Français';
        }
        return $text;
    }
}
