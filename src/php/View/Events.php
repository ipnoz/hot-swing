<?php

require_once PATH_APP.'php/dbConnection.php';

use App\Repository\EventRepository;
use App\Handler\EventOutputHandler;
use App\DTO\EventOutput;

/**
 * Artiste UID for the Hot Swing Sextet in melodinote db.
 */
const HSS_MELODINOTE_UID = 'a:1:{i:0;s:3:"107";}';
/**
 * Concert duration before considering it as passed
 */
const HSS_CONCERT_DURATION = 8*60*60;
/**
 * Path to images to use as icons.
 */
$icons = [
    'img/musiciens/Jericho.jpg',
    'img/musiciens/Franck.jpg',
    'img/musiciens/Ludovic.jpg',
    'img/musiciens/Thibaud.jpg',
    'img/musiciens/Erwann.jpg',
    'img/musiciens/Bertrand.jpg'
];

/**
 * A valid db connection is required
 */
if (null === $db) {
    return;
}

/**
 * Db query
 */
$repository = new EventRepository($db, $tables_prefix);
$results = $repository->findAll();

$results = regroupWpPostmetaDatas($results);
$handler = new EventOutputHandler(
    HSS_MELODINOTE_UID, HSS_CONCERT_DURATION, $limit, $past, $icons);
$results = $handler->process($results);

$events = [];
foreach($results as $result) {
    $event = new EventOutput(
        $result['day'],
        $result['month'],
        $result['year'],
        $result['time'],
        htEnc($result['lieu_event_descriptif_event']),
        htEnc($result['lieu_event']),
        htEnc($result['lieu_event_adresse_lieu_event']),
        htEnc($result['lieu_event_ville_event']),
        htEnc($result['url_buyTickets']),
        htEnc($result['url_contact']),
        htEnc($result['url_event']),
        $result['iconPath']
    );
    $events [] = $event;
}

foreach ($events as $event): ?>
            <div class="event wow fadeInRight">
                <div class="date-area">
                    <h3><?= $event->getDay(); ?></h3>
                    <h6 class="month"><?= $LANG->L('month_'.$event->getMonth()); ?></h6>
                    <h5><?= $event->getYear(); ?></h5>
                </div>
                <h4 class="time-area"><?= $event->getTime(); ?></h4>
                <div class="description-area">
                    <div class="img-area">
                        <img src="<?= $event->getIcon(); ?>" alt="concert-icon"/>
                    </div>
                    <div class="infos-area">
                        <h4><?= $event->getTitle(); ?></h4>
                        <h5><?= $event->getVenue(); ?></h5>
                        <h5><?= $event->getAddress(); ?></h5>
                        <h5><?= $event->getCity(); ?></h5>
                    </div>
                </div>
                <div class="buttons-area">
<?php if (!empty($event->getUrlBuyTicket())): ?>
                    <a href="<?= $event->getUrlBuyTicket(); ?>" class="btn musica-btn"><?= $LANG->L('reserve'); ?></a>
<?php endif; ?>
<?php if (!empty($event->getUrlContact())): ?>
                    <a href="<?= $event->getUrlContact(); ?>" class="btn musica-btn btn-3"><?= $LANG->L('promoter'); ?></a>
<?php endif; ?>
<?php if (!empty($event->getUrlEvent())): ?>
                    <a href="<?= $event->getUrlEvent(); ?>" class="btn musica-btn btn-3"><?= $LANG->L('event'); ?></a>
<?php endif; ?>
                </div>
            </div>
<?php endforeach; ?>
