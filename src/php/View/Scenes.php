<?php

use App\Repository\SceneRepository;

$repository = new SceneRepository();

$leftScenesTable = $repository->findAllFrance();
$rightScenesTable = $repository->findAllInternational();

function sanitizeResults(array $results): array
{
    $list = [];

    foreach ($results as $scene) {
        $scene = htEnc(trim($scene));
        if (! empty($scene)) {
            $list[] = $scene;
        }
    }

    return $list;
}

$leftScenesTable = sanitizeResults($leftScenesTable);
$rightScenesTable = sanitizeResults($rightScenesTable);

?>

            <div class="reference-scene">
                <ul class="key-notes">
                    <h6>France</h6>
<?php foreach ($leftScenesTable as $scene): ?>
                    <li>
                        <i class="fas fa-music"></i><?= $scene, PHP_EOL; ?>
                    </li>
<?php endforeach; ?>
                </ul>
                <ul class="key-notes">
                    <h6>International</h6>
<?php foreach ($rightScenesTable as $scene): ?>
                    <li>
                        <i class="fas fa-music"></i><?= $scene, PHP_EOL; ?>
                    </li>
<?php endforeach; ?>
                </ul>
            </div>
