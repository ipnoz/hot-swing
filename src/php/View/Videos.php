<?php

use App\Repository\VideoRepository;
use App\Handler\videosOutputHandler;
use App\DTO\VideoOutput;

$repository = new VideoRepository();
$results = $repository->findAll();

$handler = new videosOutputHandler();
$results = $handler->process($results);

$videos = [];
foreach ($results as $result) {
    $video = new VideoOutput(
        $result->uri,
        $result->thumbnail,
        $result->title,
        $result->description,
        $result->number
    );
    $videos[] = $video;
}

foreach ($videos as $video): ?>
            <div class="video-card">
                <img src="<?= $video->getThumbnail(); ?>" alt=""/>
                <div class="video-info">
                    <div>
                        <div class="number"><?= $video->getNumber(); ?></div>
                        <h5><?= $video->getTitle(); ?></h5>
                        <p><?= $video->getDescription(); ?></p>
                    </div>
                    <a href="<?= $video->getUri(); ?>" data-fancybox>
                        <i class="fab fa-youtube"></i>
                    </a>
                </div>
            </div>
<?php endforeach; ?>

