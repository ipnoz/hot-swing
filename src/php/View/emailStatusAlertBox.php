<?php

if (!isset($_GET['sentStatus'])) {
    return;
}

$status = getAlertCss($_GET['sentStatus']);
$message = $LANG->L('message_sent_'.$status);

?>
                        <div class="alert alert-<?= $status; ?> wow zoomIn" role="alert">
                            <?= $message, PHP_EOL; ?>
                        </div>
