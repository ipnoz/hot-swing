<?php

namespace App\DTO;

class VideoOutput
{
    private $uri;
    private $thumbnail;
    private $title;
    private $description;
    private $number;

    public function __construct($uri, $thumbnail, $title, $description, $number)
    {
        $this->uri = $uri;
        $this->thumbnail = $thumbnail;
        $this->title = $title;
        $this->description = $description;
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }
}
