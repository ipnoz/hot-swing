<?php

namespace App\DTO;

class EventOutput
{
    private $day;
    private $month;
    private $year;
    private $time;
    private $title;
    private $venue;
    private $address;
    private $city;
    private $url_buyTicket;
    private $url_contact;
    private $url_event;
    private $icon;

    public function __construct(
        $day, $month, $year, $time, $title, $venue, $address, $city,
        $url_buyTicket, $url_contact, $url_event, $icon)
    {
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
        $this->time = $time;
        $this->title = $title;
        $this->venue = $venue;
        $this->address = $address;
        $this->city = $city;
        $this->url_buyTicket = $url_buyTicket;
        $this->url_contact = $url_contact;
        $this->url_event = $url_event;
        $this->icon = $icon;
    }

    public function getDay()
    {
        return $this->day;
    }

    public function getMonth()
    {
        return $this->month;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getVenue()
    {
        return $this->venue;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getUrlBuyTicket()
    {
        return $this->url_buyTicket;
    }

    public function getUrlContact()
    {
        return $this->url_contact;
    }

    public function getUrlEvent()
    {
        return $this->url_event;
    }

    public function getIcon()
    {
        return $this->icon;
    }
}
