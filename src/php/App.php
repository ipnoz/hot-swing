<?php

setlocale(LC_ALL, 'fr_FR.utf8');

define('PATH_APP', realpath(__DIR__.'/../').'/');
define('PATH_APP_ENV', PATH_APP.'../env.php');

if (!is_readable(PATH_APP_ENV)) {
    die('<span style="color: red;">Fatal error: can\' read '.PATH_APP_ENV.'</span>');
}

require_once PATH_APP_ENV;
require_once PATH_APP.'/php/functions.php';
require_once PATH_APP.'/php/language.php';

$LANG = new language(PATH_APP.'php/locales/', 'fr');

/**
 * Handle (POST) commands
 */
if (isset($_POST['cmd'])) {
    // Handle email contact
    if ($_POST['cmd'] === 'send_email_contact') {
        require_once 'Handler/ContactEmailHandler.php';
    }
}
