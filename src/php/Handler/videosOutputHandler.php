<?php

namespace App\Handler;

/**
 * Class videosOutputHandler
 *
 * Prepare datas from persistance for output.
 */
class videosOutputHandler
{
    private $videos = [];
    /**
     * @param array $results
     * @return array
     */
    public function process(array $results)
    {
        $i = 1;
        foreach ($results as $line) {

            $line = trim($line);

            // Ignore empty lines
            if ($line === '') {
                continue;
            }

            // End of video parameters
            if ($line === 'end') {
                if (isset($video)) {
                    $this->videos[] = $video;
                    unset($video);
                }
                continue;
            }

            // Get keys and parameters of a video
            list($key, $parameter) = explode('=', $line, 2);

            $key = trim($key);
            $parameter = trim($parameter);

            // New video parameter
            if ($key === 'url') {

                $video = new \stdClass();
                $video->uri = $parameter;
                $video->thumbnail = '';
                $video->title = '';
                $video->description = '';
                $video->number = $i;

                ++$i;
                continue;
            }

            // Thumbnail parameter
            if ($key === 'image') {
                $video->thumbnail = $parameter;
                continue;
            }

            // Title parameter
            if ($key === 'title') {
                $video->title = $parameter;
                continue;
            }

            // Description parameter
            if ($key === 'decription') {
                $video->description = $parameter;
                continue;
            }
        }

        // Add the total to all videos
        $total = count($this->videos);

        foreach ($this->videos as &$video) {
            $video->number .= '/'.$total;
        }

        return $this->videos;
    }
}
