<?php

define('MAIL_EOL', "\r\n");

/**
 * Redirection to main
 */
function redirectToMain($result, bool $AJAX)
{
    if ($AJAX) {
        die($result);
    }
    header('Location:contact.php?sentStatus='.$result);
    die();
}

$isAjax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest');

/**
 * HTML submission validator
 */
if (
    $_POST['name'] === ''
    OR $_POST['email'] === ''
    OR $_POST['message'] === ''
    OR ! isset($_POST['g-recaptcha-response'])
    OR $_POST['g-recaptcha-response'] === ''
){
    redirectToMain('error', $isAjax);
}

if (! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    redirectToMain('error', $isAjax);
}

$context = stream_context_create([
    'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query([
            'secret' => RECAPTCHA_SECRET_KEY,
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ])
    ]
]);

$ApiResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);

if (false === $ApiResponse) {
    redirectToMain('error', $isAjax);
}

$ApiResponse = json_decode($ApiResponse);

if (! isset($ApiResponse->success) OR true !== $ApiResponse->success) {
    redirectToMain('error', $isAjax);
}

$allowedDomains = ['hotswingsextet.com', 'www.hotswingsextet.com'];
if (HSS_DEBUG) {
    $allowedDomains[] = 'localhost';
}

if (! in_array($ApiResponse->hostname, $allowedDomains)) {
    redirectToMain('error', $isAjax);
}

$name = htEnc(strip_tags($_POST['name']));
$email_address = htEnc(strip_tags($_POST['email']));
$subject = htEnc(strip_tags($_POST['subject']));
$message = htEnc(strip_tags($_POST['message']));

/**
 * Create the email and send the message
 */
$headers = 'From: '.$email_address.MAIL_EOL;
$headers .= 'Content-Type: text/html; charset="UTF-8"'.MAIL_EOL;
$email_subject = 'Message depuis '.$_SERVER['HTTP_HOST'].': '.$subject;

$email_body = '<!DOCTYPE html>'.MAIL_EOL;
$email_body .= '<html>'.MAIL_EOL;
$email_body .= '<head></head>'.MAIL_EOL;
$email_body .= '<body>'.MAIL_EOL;
$email_body .= 'De: '.$name.'<br/>'.MAIL_EOL;
$email_body .= 'Sujet: '.$subject.'<br/><br/>'.MAIL_EOL.MAIL_EOL;
$email_body .= '<pre>'.$message.'</pre>'.MAIL_EOL;
$email_body .= '--<br/>'.MAIL_EOL;
$email_body .= '<small style="font-size: 9px;">Cordialement, le HotSwingSextet: un groupe de musique qui swing</small>'.MAIL_EOL;
$email_body .= '</body>'.MAIL_EOL;
$email_body .= '</html>'.MAIL_EOL;

$result = mail(
    HSS_CONTACT_EMAIL,
    $email_subject,
    $email_body,
    $headers
);

redirectToMain($result ? 'success' : 'error', $isAjax);
