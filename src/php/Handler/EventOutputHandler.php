<?php

namespace App\Handler;

/**
 * Class EventOutputHandler
 *
 * Handle with melodinote db concerts datas
 */
class EventOutputHandler
{
    private $artisteUid;
    private $duration;
    private $limit;
    private $findPastEvent;
    private $iconsPaths;

    public function __construct(
        $artisteUid, int $duration, $limit, bool $past, array $icons)
    {
        $this->artisteUid = $artisteUid;
        $this->duration = $duration;
        $this->limit = $limit;
        $this->findPastEvent = $past;
        $this->iconsPaths = $icons;
    }

    /**
     * Order events by date.
     */
    private function compare_date_event($a, $b)
    {
        return strcmp($a['date_event'], $b['date_event']);
    }

    public function process(array $results)
    {

        // sort
        usort($results, [$this, 'compare_date_event']);
        
        // Inverse sort
        if ($this->findPastEvent) {
            $results = array_reverse($results);
        }

        shuffle($this->iconsPaths);
        $events = [];
        $i = 0;

        foreach($results as $result) {
            /**
             * Check if limit is reached
             */
            if (is_int($this->limit) && $this->limit === $i) {
                break;
            }
            /**
             * Process results only for the artiste UID
             */
            if ($result['Artiste_event'] !== $this->artisteUid) {
                continue;
            }
            /**
             * Remember: this is just the day/month/year date.
             * Time is set to 00h00.
             */
            $result['start'] = strToTime($result['date_event']);

            /**
             * Melodinote add the time information in the title of the concert in the
             * Worpdress web site.
             * Grep the concert time from "lieu_event_descriptif_event"
             */
            $explode = explode(' - ', $result['lieu_event_descriptif_event']);
            $last = trim(end($explode));

            if (1 === preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3])[h|H|:][0-5][0-9]$/', $last)) {
                $result['time'] = str_replace(['H','h'], ':', $last);
                array_pop($explode);
                $result['lieu_event_descriptif_event'] = join(' - ', $explode);
                // Add time to the date timestamp
                list($hours, $minutes) = explode(':', $result['time']);
                $result['start'] += $hours * 3600; // Hours to secondes
                $result['start'] += $minutes * 60; // Minutes to secondes
            }

            /**
             * Set the event end timestamp to user duration.
             * If no time is set yet (00h00), set the end to the next day,
             * 5 AM (+29 hours)
             */
            $resultEnd = $result['start'] + (isset($result['time']) ? $this->duration : (29*60*60));

            if (true === $this->findPastEvent) {
                if ($resultEnd > time()) {
                    continue;
                }
            } else {
                if ($resultEnd < time()) {
                    continue;
                }
            }

            /**
             * Setup event datas
             */
            $event = [];

            $event['day'] = date('d', $result['start']);
            $event['month'] = strftime('%m', $result['start']);
            $event['year'] = '';
            if (date('Y', $result['start']) > date('Y') OR $this->findPastEvent) {
                $event['year'] = date('Y', $result['start']);
            }
            $event['time'] = isset($result['time']) ? date('H:i', $result['start']) : '';

            $event['lieu_event_descriptif_event'] = $result['lieu_event_descriptif_event'];
            $event['lieu_event'] = $result['lieu_event'];
            $event['lieu_event_adresse_lieu_event'] = $result['lieu_event_adresse_lieu_event'];
            $event['lieu_event_ville_event'] = $result['lieu_event_ville_event'];

            $event['url_buyTickets'] = '';
            $event['url_contact'] = '';
            $event['url_event'] = '';

            if (isset($result['url_evenement']) && ! $this->findPastEvent) {
                $event['url_buyTickets'] = $result['url_evenement'];
            }
            if (isset($result['url_lien'])) {
                $event['url_contact'] = $result['url_lien'];
            }
            if (isset($result['url_facebook'])) {
                $event['url_event'] = $result['url_facebook'];
            }

            /**
             * Add an icon
             */
            if (!isset($this->iconsPaths[$i])) {
                $i = 0; // Reset $i if all icons have been already used.
            }
            $event['iconPath'] = $this->iconsPaths[$i];
            ++$i;

            $events[] = $event;
        }
        return $events;
    }
}
