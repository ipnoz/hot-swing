<?php

namespace App\Repository;

class SceneRepository extends RepositoryFile
{
    public function findAllFrance(): array
    {
        return file('datas/references_scenes_france.txt');
    }

    public function findAllInternational(): array
    {
        return file('datas/references_scenes_international.txt');
    }
}
