<?php

namespace App\Repository;

use \PDO;

class RepositoryDb extends Repository
{
    /**
     * @var PDO - database object
     */
    protected $db;
    /**
     * @var string - prefix of the repository table
     */
    protected $tablePrefix;

    public function __construct(PDO $db, $prefix = '')
    {
        $this->db = $db;
        $this->tablePrefix = $prefix;
    }
}
