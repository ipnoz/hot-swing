<?php

namespace App\Repository;

class Repository
{
    /**
     * @var array - results to return after a query
     */
    protected $results = [];
}
