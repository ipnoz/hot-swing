<?php

namespace App\Repository;

use \PDO;
use \PDOException;
use \PDOStatement;

class EventRepository extends RepositoryDb
{
    /**
     * @return array
     */
    public function findAll()
    {
        $sql =
            'SELECT
                id,
                menu_order,
                post_status,
                post_title,
                post_id,
                post_type,
                meta_key,
                meta_value
            FROM
                '.$this->tablePrefix.'posts t1
            INNER JOIN
                '.$this->tablePrefix.'postmeta v1
            ON
                t1.id = v1.post_id
            WHERE
                post_type = \'concert_spectacle\'
            AND
                post_status = \'publish\'';

        $results = null;

        try {
            $results = $this->db->query($sql);
        }
        catch(PDOException $e) { }

        if ($results instanceof PDOStatement) {
            $this->results = $results->fetchAll(PDO::FETCH_ASSOC);
        }

        return $this->results;
    }
}
