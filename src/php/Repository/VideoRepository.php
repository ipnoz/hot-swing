<?php

namespace App\Repository;

class VideoRepository extends RepositoryFile
{
    /**
     * @return array
     */
    public function findAll()
    {
        $this->results = file(PATH_APP.'datas/videos.txt');
        return $this->results;
    }
}
