<?php

/**
 * Autoload namespace helper
 *
 * @param $class
 */
function __autoload($class)
{
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    $path = str_replace('App', '', $path);
    require_once(PATH_APP.'php/'.$path.'.php');
}

/**
 * Print debug HTML error message helper
 *
 * @param $message - string
 */
function debugError($message)
{
    echo '<div class="debugError">Debug error: '.htEnc($message).'<div>';
}

/**
 * htmlEntities helper
 *
 * @param $str - string to convert
 * @return string
 */
function htEnc($str)
{
    return htmlEntities($str, ENT_QUOTES, 'UTF-8');
}

/**
 * getAlertCss helper
 *
 * Return bootstrap css alert class
 */
function getAlertCss($result) {
    if ($result === 'success') {
        $css = 'success';
    } else {
        $css = 'error';
    }
    return $css;
}

/**
 * Regroup datas from wordpress table Postmeta with related datas from Post table
 *
 * @param array $results
 * @return array $regroup
 */
function regroupWpPostmetaDatas(array $results)
{
    $id = null;
    $regroup = [];

    foreach ($results as $result) {
        if ($id === $result['id']) {
            // Update event with others wordpress postmeta datas
            $regroup[$id][$result['meta_key']] = $result['meta_value'];
        } else {
            // New entry
            $entry = [];
            $entry['id'] = $result['id'];
            $entry['title'] = $result['post_title'];
            $entry[$result['meta_key']] = $result['meta_value'];
            $regroup[$result['id']] = $entry;
        }
        $id = $result['id'];
    }
    return $regroup;
}
