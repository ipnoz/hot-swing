<?php

// Menu
$LANG['menu_insta'] = 'Notre page Instagram';
$LANG['menu_fb'] = 'Notre page Facebook';
$LANG['menu_snd'] = 'Notre page Soundcloud';
$LANG['menu_itu'] = 'Notre page Itunes';
$LANG['menu_yt'] = 'Notre page Youtube';
$LANG['menu_bp'] = 'Notre page Bandcamp';

// Index page
$LANG['bio_title'] = 'Des artistes bio';
$LANG['bio_subtitle'] = 'Elevés en plein air de musique.';
$LANG['biography'] = 'Ce groupe de musique vous propose de remonter le temps pour une virée dans l’ambiance
                        frénétique des clubs de jazz des années 30.
                        Entre <strong>Paris</strong> et <strong>Harlem</strong>, <strong>Django Reinhardt</strong>
                        et <strong>Duke Ellington</strong>, en passant par <strong>Louis Armstrong</strong>, Le
                        <strong>Hot Swing Sextet</strong> a organisé pour vous, un voyage « sur mesure »,
                        direction : « le Swing ».

                        Embarquez avec ces six jeunes musiciens débordant d’énergie. « Balancement »,
                        « déhanchement » et bonne humeur sont au programme de cette grande traversée du
                        « middle-jazz ».
                        Et maintenant place à la fête, avec le
';

$LANG['trumpeter'] = 'Trompettiste';
$LANG['saxophonist'] = 'Saxophoniste';
$LANG['guitarist'] = 'Guitariste';
$LANG['bassist'] = 'Contrebassiste';
$LANG['drummer'] = 'Batteur';

$LANG['comming_next'] = 'À venir';
$LANG['all_concerts'] = 'Voir toutes nos dates de concert';
$LANG['all_past_concerts'] = 'Voir nos concerts passés';
$LANG['our_last_concerts'] = 'Nos concerts passés';

$LANG['reserve'] = 'Réserver';
$LANG['promoter'] = 'Organisateur';
$LANG['event'] = 'Évenement';

$LANG['videos'] = 'Vidéos';
$LANG['buy_on'] = 'L\'album sur';

$LANG['playlist_citation'] = 'Du jazz à danser certes mais aussi à écouter grâce aux arrangements originaux et à l’interprétation parfaite';

// Pro page
$LANG['breadcrumb_pro_page'] = 'Espace pro';
$LANG['breadcrumb_press_page'] = 'Dossier de presse';
$LANG['breadcrumb_technical_page'] = 'Fiche technique';
$LANG['pro_general_info'] = 'Vous trouverez ici tout ce que le groupe vous propose pour vos évènements';
$LANG['see_press_page'] = 'Voir le dossier de presse';
$LANG['see_technical_page'] = 'Voir la fiche technique';
$LANG['HD_photo_to_print'] = 'Visuel d\'affiche HD à imprimer';
$LANG['clic_for_download'] = 'Cliquez sur l\'image pour la télécharger';
$LANG['size_and_weight'] = 'Taille: %s pixels, %s';
$LANG['references_scenes'] = 'Références scènes';
$LANG['download_press_file'] = 'Télécharger le PDF du dossier de presse';
$LANG['download_technical_file'] = 'Télécharger la fiche technique';


// Contact page
$LANG['your_name'] = 'Votre nom';
$LANG['your_email'] = 'Votre email';
$LANG['subject'] = 'Sujet';
$LANG['send'] = 'Envoyer';
$LANG['contact_us_message'] = 'Utilisez le formulaire pour contacter les musiciens directement';
$LANG['message_sent_success'] = 'Le message a été envoyé aux musiciens';
$LANG['message_sent_error'] = 'Une erreur est survenue durant le processus, le message n\'a pas été envoyé';

// Footer
$LANG['follow_us'] = 'Suivez-nous';
$LANG['contact_us'] = 'Nous contacter';
$LANG['scene_tour_concert'] = 'Scène, tournée, concert';
$LANG['technical_control'] = 'Régie technique';

$LANG['developed_by'] = 'Site élaboré par ';

// Misc
$LANG['month_01'] = 'janvier';
$LANG['month_02'] = 'février';
$LANG['month_03'] = 'mars';
$LANG['month_04'] = 'avril';
$LANG['month_05'] = 'mai';
$LANG['month_06'] = 'juin';
$LANG['month_07'] = 'juillet';
$LANG['month_08'] = 'août';
$LANG['month_09'] = 'septembre';
$LANG['month_10'] = 'octobre';
$LANG['month_11'] = 'novembre';
$LANG['month_12'] = 'décembre';


