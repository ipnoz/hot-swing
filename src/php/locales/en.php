<?php

// Menu
$LANG['menu_insta'] = 'Our Instagram page';
$LANG['menu_fb'] = 'Our Facebook page ';
$LANG['menu_snd'] = 'Our Soundcloud page';
$LANG['menu_itu'] = 'Our Itunes page';
$LANG['menu_yt'] = 'Our Youtube page';
$LANG['menu_bp'] = 'Our Bandcamp page';

// Index page
$LANG['bio_title'] = 'Organic artists';
$LANG['bio_subtitle'] = 'Raised outdoors with music';
$LANG['biography'] = 'This group offers a trip back in time in the frenzied atmosphere of the jazz clubs of the 30s.
                        From <strong>Paris</strong> to <strong>Harlem</strong>, From <strong>Django Reinhardt</strong>
                         to <strong>Duke Ellington</strong>, <strong>Louis Armstrong</strong> through, 
                        <strong>Hot Swing Sextet</strong> organized for you, a trip « tailored » towards « Swing ».

                        Get on board with these six young musicians full of energy. « Swing, » « swaying » and good 
                        humor are on the menu of this great Journey through the « middle-jazz ».
                        Let the party begin with the
';

$LANG['trumpeter'] = 'Trumpeter';
$LANG['saxophonist'] = 'Saxophonist';
$LANG['guitarist'] = 'Guitarist';
$LANG['bassist'] = 'Bassist';
$LANG['drummer'] = 'Drummer';

$LANG['comming_next'] = 'Comming next';
$LANG['all_concerts'] = 'See all concerts incomming';
$LANG['all_past_concerts'] = 'See all past concerts';
$LANG['our_last_concerts'] = 'Our last concerts';

$LANG['reserve'] = 'Reserve';
$LANG['promoter'] = 'Promoter';
$LANG['event'] = 'Event';

$LANG['videos'] = 'Videos';
$LANG['buy_on'] = 'Album on';

// Pro page
$LANG['breadcrumb_pro_page'] = 'Page pro';
$LANG['breadcrumb_press_page'] = 'Press file';
$LANG['breadcrumb_technical_page'] = 'Technical file';
$LANG['pro_general_info'] = 'You can find here all you need for your events';
$LANG['see_press_page'] = 'See press page';
$LANG['see_technical_page'] = 'See technical page';
$LANG['HD_photo_to_print'] = 'Visual poster HD to print';
$LANG['clic_for_download'] = 'Click on the image to download';
$LANG['size_and_weight'] = 'Size: %s pixels, %s';
$LANG['references_scenes'] = 'References scenes';
$LANG['download_press_file'] = 'Download press PDF file';
$LANG['download_technical_file'] = 'Download technical file';

// Contact page
$LANG['your_name'] = 'Your name';
$LANG['your_email'] = 'Your email';
$LANG['subject'] = 'Subject';
$LANG['send'] = 'Send';
$LANG['contact_us_message'] = 'Use this form to contact the musicians directly';
$LANG['message_sent_success'] = 'The message has been sent to the musicians';
$LANG['message_sent_error'] = 'An error occurred during the process, the message was not sent';

// Footer
$LANG['follow_us'] = 'Follow us';
$LANG['contact_us'] = 'Contact us';
$LANG['scene_tour_concert'] = 'Scene, tour, concert';
$LANG['technical_control'] = 'Technical control';

$LANG['developed_by'] = 'Website developed by ';

// Misc
$LANG['month_01'] = 'january';
$LANG['month_02'] = 'february';
$LANG['month_03'] = 'march';
$LANG['month_04'] = 'april';
$LANG['month_05'] = 'may';
$LANG['month_06'] = 'june';
$LANG['month_07'] = 'july';
$LANG['month_08'] = 'august';
$LANG['month_09'] = 'september';
$LANG['month_10'] = 'october';
$LANG['month_11'] = 'november';
$LANG['month_12'] = 'december';
