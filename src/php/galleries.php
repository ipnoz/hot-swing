<?php

/**
 * Scan a directory and find all images in it with extentions
 */
function scanDirForImages($path) {

    $list = array();
    $searchForExtention = ['jpg', 'png', 'jpeg'];
    $scan = scandir($path);

    foreach ($scan as $file) {

        $explode = explode('.', $file);
        $extention = array_pop($explode);
        $title = join('.', $explode);

        if (in_array($extention, $searchForExtention)) {

            $img = new stdClass();

            $img->path = $path.'/'.$file;
            $img->thumbnail = $path.'/thumbs/'.$file;
            $img->fileName = $file;
            $img->title = $title;
            $img->extension = $extention;

            $list[] = $img;
        }
    }

    return $list;
}

$topGallery = scanDirForImages('datas/posters-w400/top');
$bottomGallery = scanDirForImages('datas/posters-w400/bottom');
