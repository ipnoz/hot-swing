<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area bg-overlay2"></div>

    <div class="bg-gradients"></div>

    <!-- Concerts section -->
    <section id="concerts">

        <div class="container">

            <div class="section-heading">
                <h6 class="wow"><?= $LANG->L('our_last_concerts'); ?></h6>
            </div>

<?php $limit = false; $past = true; require PATH_APP.'php/View/Events.php'; ?>

        </div>
    </section>

    <div class="bg-gradients"></div>

    <!-- @@include cta-section.inc.html {"css":"concerts"} -->
<!-- @@close -->
