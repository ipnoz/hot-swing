<!-- @@master = master.php -->

<!-- @@block = scripts -->
    <script>let RECAPTCHA_PUBLIC_KEY = '<?= RECAPTCHA_PUBLIC_KEY; ?>'</script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script src="js/recaptcha.min.js"></script>
<!-- @@close -->

<!-- @@block = content -->

    <script type="text/javascript">
        var LANG = {
            success: "<?= $LANG->L('message_sent_success'); ?>",
            error: "<?= $LANG->L('message_sent_error'); ?>"
        };
    </script>

    <div class="breadcumb-area contact"></div>

    <div class="bg-gradients"></div>

    <section id="contact-section">

        <div class="container-fluid overflowx-hidden">

            <div class="row">

                <div class="video-area">
                    <div class="video-bg">
                        <video id="teaser-vid" src="video/Teaser.mp4" class="wow zoomIn" data-wow-delay="1s" autoplay muted></video>
                    </div>
                </div>

                <div class="contact-area">

                    <div class="container">

<?php require_once 'php/View/emailStatusAlertBox.php'; ?>

                        <h3 class="title wow zoomIn mb-4" data-wow-delay="0.5s">
                            <?= $LANG->L('contact_us'), PHP_EOL; ?>
                        </h3>

                        <div class="wow zoomIn" data-wow-delay="0.75s">
                            <?= $LANG->L('contact_us_message'), PHP_EOL; ?>
                        </div>

                        <form id="contact" method="post">
                            <input type="hidden" name="cmd" value="send_email_contact"/>
                            <div class="form-group wow zoomIn" data-wow-delay="1s">
                                <input type="text" class="form-control" name="name" placeholder="<?= $LANG->L('your_name'); ?>" required="required"/>
                            </div>
                            <div class="form-group wow zoomIn" data-wow-delay="1.25s">
                                <input type="email" class="form-control" name="email" placeholder="<?= $LANG->L('your_email'); ?>" required="required"/>
                            </div>
                            <div class="form-group wow zoomIn" data-wow-delay="1.50s">
                                <input type="text" class="form-control" name="subject" placeholder="<?= $LANG->L('subject'); ?>"/>
                            </div>
                            <div class="form-group wow zoomIn" data-wow-delay="1.75s">
                                <textarea name="message" class="form-control" cols="30" rows="10" placeholder="Message"
                                          required="required" maxlength="999"></textarea>
                            </div>

                            <div id="recaptcha" class="form-group g-recaptcha"></div>

                            <div class="text-right">
                                <button class="btn musica-btn wow zoomIn" type="submit">
                                    <?= $LANG->L('send'), PHP_EOL; ?>
                                </button>
                            </div>
                        </form>

                        <div class="ajax-alert alert" role="alert"></div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="bg-gradients-inverse"></div>

    <!-- @@include cta-section.inc.html {"css":"contact"} -->
<!-- @@close -->
