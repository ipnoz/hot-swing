/*!
 * PlaylistEr.js
 * Copyright (c) 2019 David Dadon - https://www.ipnoz.net ; Licensed MIT
 *
 * Based on this script: https://codepen.io/markhillard/pen/Hjcwu
 * Modified for my needs
 */

(function ($) {

    'use strict';

    $.fn.playlistEr = function (datas) {

        /**
         * HTML elements
         */
        var baseEl = this,
            menuEl = baseEl.children('ul'),
            audioEl = baseEl.find('audio'),
            btnPrevEl = baseEl.find('.btnPrev'),
            btnNextEl = baseEl.find('.btnNext'),
            btnPlayEl = baseEl.find('.btnPlay'),
            currentSongEl = baseEl.find('.current-song-infos');

        /**
         * Fontawsome icons
         */
        var listPlayIcon = 'fa-play-circle',
            listPauseIcon = 'fa-pause-circle',
            playerPlayBtnIcon = 'fa-play',
            playerPauseBtnIcon = 'fa-pause';

        if (! !!document.createElement('audio').canPlayType) {

            // Hide playlist and display audio tag error text.
            $(menuEl).hide();
            $(baseEl).prepend('<p>'+ $('audio').text() +'</p>');

        } else {

            var tracksPath = datas.tracksPath,
                tracks = datas.tracks,
                tracksTotal = tracks.length,
                index = 0,
                playing = false,
                extension = '',
                maxLineOnPlaylist = 8,
                buildPlaylist = $(tracks).each(function(key, value) {
                    $(menuEl).append('\
                        <li> \
                            <i class="far '+listPlayIcon+'" aria-hidden="true"></i> \
                            <span class="track">'+value.track+'</span> \
                            <span class="title">'+value.name+'</span> \
                            <span class="duration">'+value.duration+'</span> \
                        </li>');
                }),
                slicePlaylist = function () {

                    var cut, start, end;

                    if (maxLineOnPlaylist > tracksTotal) {
                        maxLineOnPlaylist = tracksTotal;
                    }

                    cut = Math.floor(maxLineOnPlaylist / 2);
                    start = index - cut;
                    if (start < 0) {
                        start = 0;
                    }
                    end = start + maxLineOnPlaylist;
                    if (end >= tracksTotal) {
                        start = tracksTotal - maxLineOnPlaylist;
                    }
                    $(menuEl).find('li').hide().slice(start, end).show();
                },
                audio = $(audioEl).on('play', function () {
                    playing = true;
                    togglePlayButtonIcons(index);
                }).on('pause', function () {
                    playing = false;
                    togglePlayButtonIcons(index);
                }).on('ended', function () {
                    if ((index + 1) < tracksTotal) {
                        index++;
                        playTrack(index);
                    } else {
                        audio.pause();
                        index = 0;
                        loadTrack(index);
                    }
                    togglePlayButtonIcons(index);
                }).get(0),
                btnPrevAction = $(btnPrevEl).on('click', function () {
                    if ((index - 1) > -1) {
                        index--;
                    } else {
                        index = tracksTotal - 1;
                    }
                    loadTrack(index);
                    if (playing) {
                        audio.play();
                    }
                }),
                btnNextAction = $(btnNextEl).on('click', function () {
                    if ((index + 1) < tracksTotal) {
                        index++;
                    } else {
                        index = 0;
                    }
                    loadTrack(index);
                    if (playing) {
                        audio.play();
                    }
                }),
                btnPlayAction = $(btnPlayEl).on('click', function () {
                    if (playing) {
                        audio.pause();
                    } else {
                        audio.play();
                    }
                }),
                btnLiAction = $(menuEl).find('li i').on('click', function () {
                    var id = parseInt($(this).parent().index());
                    if (id !== index) {
                        playTrack(id);
                    } else {
                        if (playing) {
                            audio.pause();
                        } else {
                            audio.play();
                        }
                    }
                }),
                loadTrack = function (id) {
                    $(menuEl).children('li.selected').removeClass('selected');
                    $(menuEl).children('li:eq('+ id +')').addClass('selected');
                    $(currentSongEl).children('.track').text(tracks[id].track);
                    $(currentSongEl).children('.title').text(tracks[id].name);
                    $(currentSongEl).children('.duration').text(tracks[id].duration);
                    index = id;
                    audio.src = tracksPath + tracks[id].file + extension;
                    slicePlaylist();
                },
                playTrack = function (id) {
                    loadTrack(id);
                    audio.play();
                },
                togglePlayButtonIcons = function (id) {
                    $(menuEl).find('li i').addClass(listPlayIcon).removeClass(listPauseIcon);
                    if (playing) {
                        $(menuEl).find('li:eq('+ id +') i').addClass(listPauseIcon).removeClass(listPlayIcon);
                        $(btnPlayEl).children('i').addClass(playerPauseBtnIcon).removeClass(playerPlayBtnIcon);
                    } else {
                        $(btnPlayEl).children('i').addClass(playerPlayBtnIcon).removeClass(playerPauseBtnIcon);
                    }
                };

            extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
            loadTrack(index);
        }
    }
}(jQuery));
