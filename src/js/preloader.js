(function ($) {

    'use strict';

    var preloaderEl = '#preloader';

    /**
     * This action prove that browser use javascript
     */
    $(preloaderEl).addClass('jsIsActived');

    /**
     * Preloader
     */
    $(window).on('load', function () {
        $(preloaderEl).fadeOut('slow', function () {
            $(this).remove();
        });
    });

    /**
     * Timeout
     * Set a timeout of 5 secondes to avoid to long preloading screen
     */
    $(window).ready(function() {
        setTimeout(function(){
            $(preloaderEl).fadeOut('slow', function () {
                $(this).remove();
            });
        }, 5000);
    });

})(jQuery);
