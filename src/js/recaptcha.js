
// Recaptcha must be valided by user
var recaptchaValided = false;

/*
 * reCaptcha
 */
function reCaptchaVerify(response)
{
    if (response === document.querySelector('.g-recaptcha-response').value) {
        recaptchaValided = true;
    }
}

function onRecaptchaExpired()
{
    recaptchaValided = false;
}

var onloadCallback = function() {
    grecaptcha.render('recaptcha', {
        'sitekey' : RECAPTCHA_PUBLIC_KEY,
        'callback': reCaptchaVerify,
        'expired-callback': onRecaptchaExpired
    });
};
