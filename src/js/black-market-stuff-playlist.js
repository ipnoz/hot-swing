
if ($.fn.playlistEr) {
    var blackMarketStuff = {
        "name": "Black market stuff",
        "month": "Septembre",
        "year": "2018",
        "tracksPath": "./audio/black-market-stuff-2018/",
        "tracks":
            [{
                "track": "01",
                "name": "Benny's buggle",
                "duration": "3:06",
                "file": "01-Benny-s-bugle"
            }, {
                "track": "02",
                "name": "When the quail come back to San Quentin",
                "duration": "3:09",
                "file": "02-When-the-quail-come-back-to-San-Quentin"
            }, {
                "track": "03",
                "name": "I may be wrong",
                "duration": "2:55",
                "file": "03-I-may-be-wrong"
            }, {
                "track": "04",
                "name": "Lagwood walk",
                "duration": "4:22",
                "file": "04-Lagwood-walk"
            }, {
                "track": "05",
                "name": "Rose Room",
                "duration": "2:36",
                "file": "05-Rose-room"
            }, {
                "track": "06",
                "name": "A smooth one",
                "duration": "3:21",
                "file": "06-A-smooth-one"
            }, {
                "track": "07",
                "name": "Effervescent blues",
                "duration": "2:46",
                "file": "07-Effervescent-blues"
            }, {
                "track": "08",
                "name": "N.R.C. jump",
                "duration": "4:12",
                "file": "08-N.R.C-jump"
            }, {
                "track": "09",
                "name": "Woke up clipped",
                "duration": "3:02",
                "file": "09-Woke-up-clipped"
            }, {
                "track": "10",
                "name": "9:20 special",
                "duration": "2:44",
                "file": "10-920-special"
            }, {
                "track": "11",
                "name": "Summit ridge drive",
                "duration": "3:26",
                "file": "11-Summit-ridge-drive"
            }, {
                "track": "12",
                "name": "Loose wig",
                "duration": "2:57",
                "file": "12-Loose-wig"
            }, {
                "track": "13",
                "name": "Black market stuff",
                "duration": "2:37",
                "file": "13-Black-market-stuff"
            }, {
                "track": "14",
                "name": "Saint Louis blues",
                "duration": "3:02",
                "file": "14-St.-Louis-blues"
            }, {
                "track": "15",
                "name": "Bunny",
                "duration": "3:18",
                "file": "15-Bunny"
            }]
    };
    $('#main-playlister').playlistEr(blackMarketStuff);
}
