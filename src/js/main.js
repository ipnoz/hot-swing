(function ($) {

    'use strict';

    var browserWindow = $(window);
    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    /**
     * MENU: vars
     */
    var menuEl = '#menu';
    var menuScrollOffset = 100;
    var menuToggleTime = 250;

    /**
     * MENU: Add a background to the sticky menu when scrolling down.
     */
    $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        if (scroll > menuScrollOffset ) {
            $('header').addClass('add-bg');
        } else {
            $('header').removeClass('add-bg');
        }
    });

    /**
     * MENU: Toggle open/close responsive menu
     *
     * Override jQuery.toggle() method action:
     * Remove style="display:[block|none]" injected into the HTML element
     * and add css class "show"
     */
    function toggleMenu() {
        $(menuEl).toggle(menuToggleTime, function() {
            $(this).css('display', '').toggleClass('show');
        });
    }

    /**
     * MENU: Action button for toggle open/close responsive menu
     */
    $('header button').on('click', toggleMenu);

    /**
     * Owl carousel(s)
     *
     * Owl-carousel ^2.3 do not refresh the autoplayTimeout on user slide.
     * This is a realy anoying bug.
     * Stay with 2.2.1 for now...
     * ...Or not. Owl carousel 2.2.1 have also anoying bugs...
     *
     * See:
     * https://github.com/OwlCarousel2/OwlCarousel2/issues/2452
     * https://github.com/OwlCarousel2/OwlCarousel2/issues/993
     */
    if ($.fn.owlCarousel) {

        $('.single-home-slide').removeClass('no-js-hide');
        $('.gallery-area').removeClass('no-js');

        var homeSlide = $('.home-slides');
        var musiciens = $('.about-musiciens');
        var videos = $('.videos-slides');
        var postersTop = $('.posters-top-slider');
        var postersBottom = $('.posters-bottom-slider');

        homeSlide.owlCarousel({
            items: 1,
            margin: 0,
            mouseDrag: false,
            touchDrag: false,
            loop: true,
            nav: false,
            navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
            dots: false,
            autoplay: true,
            autoplayTimeout: 7000,
            smartSpeed: 1000,
            checkVisible: false,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut'
        });

        homeSlide.on('translate.owl.carousel', function () {
            var slideLayer = $("[data-animation]");
            slideLayer.each(function () {
                var anim_name = $(this).data('animation');
                $(this).removeClass('animated ' + anim_name).css('opacity', '0');
            });
        });

        homeSlide.on('translated.owl.carousel', function () {
            var slideLayer = homeSlide.find('.owl-item.active').find("[data-animation]");
            slideLayer.each(function () {
                var anim_name = $(this).data('animation');
                $(this).addClass('animated ' + anim_name).css('opacity', '1');
            });
        });

        $('[data-delay]').each(function () {
            var anim_del = $(this).data('delay');
            $(this).css('animation-delay', anim_del);
        });

        $('[data-duration]').each(function () {
            var anim_dur = $(this).data('duration');
            $(this).css('animation-duration', anim_dur);
        });

        musiciens.owlCarousel({
            items: 1,
            margin: 0,
            center: false,
            loop: true,
            nav: true,
            navText: ['<i class="fas fa-angle-double-left"></i>', '<i class="fas fa-angle-double-right"></i>'],
            mouseDrag: true,
            touchDrag: true,
            autoplay: false,
            checkVisible: false,
            responsive: {
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });

        videos.owlCarousel({
            margin: 45,
            loop: true,
            nav: false,
            navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
            dots: false,
            autoplay: true,
            autoplayHoverPause: true,
            // Smooth scrool
            // autoplayTimeout and autoplaySpeed must have the same value to avoid a bug at the end of the loop
            slideTransition: 'linear',
            autoplayTimeout: 5000,
            autoplaySpeed: 5000,
            checkVisible: false,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2,
                },
                992: {
                    items: 3,
                    nav: true
                },
                1499: {
                    items: 4,
                    nav: true
                }
            }
        });

        postersTop.owlCarousel({
            items: 6,
            margin: 0,
            center: true,
            loop: true,
            nav: false,
            mouseDrag: false,
            touchDrag: false,
            dots: false,
            autoplay: true,
            slideTransition: 'linear',
            autoplayTimeout: 5000,
            autoplaySpeed: 5000,
            smartSpeed: 600,
            checkVisible: false,
            lazyLoad: true,
            lazyLoadEager: 1,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 3
                },
                992: {
                    items: 6
                }
            }
        });

        postersBottom.owlCarousel({
            items: 6,
            margin: 0,
            center: true,
            loop: true,
            nav: false,
            mouseDrag: false,
            touchDrag: false,
            dots: false,
            autoplay: true,
            slideTransition: 'linear',
            autoplayTimeout: 5000,
            autoplaySpeed: 5000,
            smartSpeed: 600,
            checkVisible: false,
            rtl: true,
            lazyLoad: true,
            lazyLoadEager: 1,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 3
                },
                992: {
                    items: 6
                }
            }
        });
    }

    function loopAboutUs(){
        $('.thumbnails').find('img').toggleClass('hidden');
        setTimeout(function(){ loopAboutUs() }, 5000);
    }
    loopAboutUs();

    /**
     * Fancybox
     */
    if ($.fn.fancybox) {

        var fbSelector = '.owl-item:not(.cloned) a';

        /**
         * ================================================
         *
         * Fancybox and owl carousel bugs, see:
         * https://stackoverflow.com/questions/53759044/fancybox-with-owl-carousel-lazyload
         *
         * Workaround:
         * Attach custom click event on cloned elements,
         * trigger click event on corresponding link.
         *
         * ================================================
         */
        $(document).on('click', '.owl-item.cloned a', function() {
            var $slides = $(this).parent().siblings('.owl-item:not(.cloned)');
            $slides
                .eq( ( $(this).attr("data-index") || 0) % $slides.length )
                .find('a')
                .trigger("click.fb-start", { $trigger: $(this) });
            return false;
        });
    }

    /**
     * WOW.js
     */
    if (typeof WOW !== 'undefined') {
        var wowOffset = 0;
        // if (browserWindow.width() > 991) {
        //     wowOffset = 100;
        // }
        var wow = new WOW({
            // animateClass: 'animated fadeInUp',
            animateClass: 'animated fadeIn',
            offset: wowOffset,
            mobile: isFirefox,
            live: false,
        });
        wow.init();
    }

    /**
     * Smooth ScrollUp
     * Depends: jquery-easing
     */
    if ($.fn.scrollUp) {
        browserWindow.scrollUp({
            scrollDistance: 900,
            animation: 'none',
            animationSpeed: 900,
            scrollSpeed: 500,
            easingType: 'easeInOutExpo',
            scrollText: '<i class="fas fa-angle-up"></i>',
            zIndex: 999
        });
    }

    /**
     * Smooth scrollDown
     * Depends: jquery-easing
     */
    $('.scrollDown a[href*="#"]').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    /**
     * Smooth scrollDown to next section
     * Depends: jquery-easing
     * From this post
     * https://stackoverflow.com/questions/29760049/scroll-to-next-section/29761220#29761220
     */
    // $('.scrollDown a').click(function(){
    //
    //     var y = $('section').filter(function(i, el) {
    //         return el.getBoundingClientRect().bottom > 0;
    //     }).next('section').offset().top;
    //
    //     $('html, body').stop().animate({scrollTop: y});
    //
    // });


    /**
     * Contact video fadeOut on ended
     */
    $('#teaser-vid').on('ended', function() {
        $('#teaser-vid').parent().addClass('hide');
        $('#teaser-vid').remove();
    });


    /**
     * Contact email with AJAX
     */

    var contactEl = $('form#contact');

    // Allow one submit action
    var contactFormSubmitted = false;

    $(contactEl).submit(function(e) {

        e.preventDefault();

        if (recaptchaValided && ! contactFormSubmitted) {

            $.ajax({
                type: 'POST',
                data: $(this).serialize(),
                success: function(response) {
                    if ('success' === response) {
                        contactFormSubmitted = true;
                    }
                    $('.ajax-alert').text('').removeClass('alert-error').fadeOut(200);
                    $('.ajax-alert').text(LANG[response]).addClass('alert-'+response).fadeIn(1000);
                },
                error: function(xhr, status, error) {
                    alert('AJAX Error: '+ error +' status: '+ status + ', text: '+ xhr.responseText );
                }
            });
        }

        return false;
    });

})(jQuery);
