
if ($.fn.playlistEr) {
    var WhatsYourJivePlaylist = {
        "name": "What's your jive ?",
        "month": "December",
        "year": "2020",
        "tracksPath": "./audio/whats-your-jive-2020/",
        "tracks":
            [{
                "track": "01",
                "name": "Hoppin' John",
                "duration": "3:07",
                "file": "01-Hoppin-John"
            }, {
                "track": "02",
                "name": "Tatoe pie",
                "duration": "3:01",
                "file": "02-Tatoe-pie"
            }, {
                "track": "03",
                "name": "The goon drag",
                "duration": "3:20",
                "file": "03-The-goon-drag"
            }, {
                "track": "04",
                "name": "What's your story",
                "duration": "3:13",
                "file": "04-What-s-your-story"
            }, {
                "track": "05",
                "name": "Wham",
                "duration": "3:11",
                "file": "05-Wham"
            }, {
                "track": "06",
                "name": "Bloodhound",
                "duration": "3:21",
                "file": "06-Bloodhound"
            }, {
                "track": "07",
                "name": "T'aint me",
                "duration": "3:32",
                "file": "07-Taint-me"
            }, {
                "track": "08",
                "name": "Wine-O",
                "duration": "3:02",
                "file": "08-Wine-O"
            }, {
                "track": "09",
                "name": "Windy",
                "duration": "3:34",
                "file": "09-Windy"
            }, {
                "track": "10",
                "name": "Red dust",
                "duration": "3:37",
                "file": "10-Red-dust"
            }, {
                "track": "11",
                "name": "Fetch it to me",
                "duration": "3:12",
                "file": "11-Fetch-it-to-me"
            }, {
                "track": "12",
                "name": "Embryo",
                "duration": "2:42",
                "file": "12-Embryo"
            }, {
                "track": "13",
                "name": "Payin them dues blues",
                "duration": "3:25",
                "file": "13-Payin-them-dues-blues"
            }, {
                "track": "14",
                "name": "Ridin' and jivin'",
                "duration": "2:52",
                "file": "14-Ridin-and-jivin"
            }, {
                "track": "15",
                "name": "Just jivin' around",
                "duration": "2:49",
                "file": "15-Just-jivin-around"
            }]
    };
    $('#main-playlister').playlistEr(WhatsYourJivePlaylist);
}
