<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area bg-overlay2"></div>

    <div class="bg-gradients"></div>

    <!-- Concerts section -->
    <section id="concerts">

        <div class="container">

            <div class="section-heading">
                <h4 class="wow">On tour</h4>
                <h6 class="wow"><?= $LANG->L('comming_next'); ?></h6>
            </div>

<?php $limit = false; $past = false; require PATH_APP.'php/View/Events.php'; ?>

        </div>

        <div class="bottom-link">
            <a href="concerts-past.php<?= $LANG->getUrlParam(); ?>">
                <i class="fas fa-music" aria-hidden="true"></i>
                <?= $LANG->L('all_past_concerts'), PHP_EOL; ?>
            </a>
        </div>
    </section>

    <div class="bg-gradients"></div>

    <!-- @@include cta-section.inc.html {"css":"concerts"} -->
<!-- @@close -->
