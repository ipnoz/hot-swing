<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area bg-overlay2"></div>

    <div class="bg-gradients"></div>

    <section id="fiche-technique-section">

        <div class="container">

            <div class="breadcrumb">
                <a href="pro.php<?= $LANG->getUrlParam(); ?>">/<?= $LANG->L('breadcrumb_pro_page');?></a>/<?= $LANG->L('breadcrumb_technical_page'), PHP_EOL;?>
            </div>

            <div class="downloadBtn">
                <a class="btn musica-btn mb-2" href="datas/pro/fiche-technique.pdf" download="fiche-technique-Hot-Swing-Sextet.pdf">
                    <?= $LANG->L('download_technical_file'), PHP_EOL;?>
                </a>
            </div>

            <div class="fiche-technique">

                <p class="date">
                    27/03/2020
                </p>

                <h4 class="text-center">
                    Régie générale
                </h4>

                <ul class="regie-generale">
                    <li>
                        Franck RICHARD
                    </li>
                    <li>
                        +33 (0)6 65 26 69 87
                    </li>
                    <li>
                        <a href="mailto:frich33@gmail.com">frich33@gmail.com</a>
                    </li>
                </ul>

                <p class="important">
                    Nous vous remercions de bien vouloir prêter la plus grande attention au contenu de ce contrat technique.
                </p>

                <h5>
                    1 - Acceuil
                </h5>

                <p>
                    L’équipe se compose de 6 musiciens et se déplace généralement avec un véhicule type
                    Renault Trafic Minibus 9 places ou train depuis gare de Bordeaux-St-Jean (33) ou avion
                    depuis aéroport de Mérignac (33).
                </p>

                <h5>
                    2 - La scène
                </h5>

                <ul>
                    <li>
                        La scène doit être totalement stable et plane, de couleur sombre et unie, équipée d'une
                        jupe de scène (si possible).
                    </li>
                    <li>
                        Les accès à la scène seront stables, signalés et convenablement éclairés.
                    </li>
                    <li>
                        L’organisateur mettra à disposition le meilleur accés possible pour le chargement et le
                        déchargement des instruments.
                    </li>
                    <li>
                        <strong>6 bouteilles de 50 cl d’eau minérale</strong> seront disposées sur scène avant la représentation
                        du groupe.
                    </li>
                    <li>
                        <strong>
                            L’organisateur prévoira un emplacement avec une table pour le merchandising
                            (disques+T-shirts), visible par le public et éclairée.
                        </strong>
                    </li>
                </ul>

                <h5>
                    3 - Loge – catering
                </h5>

                <ul>
                    <li>
                        Au moins une loge fermant à clef sera mise, dès son arrivée, à la disposition exclusive
                        du groupe. Les clées seront remises à un membre du groupe.
                    </li>
                    <li>
                        Prévoir dans la loge, avant et après le concert, un catering comprenant <strong>café, thé, eau
                            chaude et fraîche, sucre, gâteaux et fruits secs, fruits de saison, jus de fruits,
                            boissons et bières fraîches</strong> ainsi que verres et serviettes, le tout en quantité
                        suffisante.
                    </li>
                </ul>

                <h5>
                    4 - Hébergements – Repas
                </h5>

                <ul>
                    <li>
                        Dans le cas où il est prévu que le groupe soit hébergé, l’organisateur prévoira un
                        hébergement pour <strong>6 personnes</strong> chez l’habitant ou dans un hôtel minimum ** avec petits
                        déjeuners inclus dans <strong>3 twin minimum</strong> (lits séparés).
                    </li>
                    <li>
                        Les repas chaud pour <strong>6 personnes</strong> seront gérés par l’organisateur et pris soit avec
                        l’équipe d’accueil (<strong style="color: red">dont 1 repas végan de préférence ou végétarien</strong>), soit dans un
                        restaurant de cuisine régionale à proximité du lieu de concert. Ils demeurent à la charge
                        de l’organisateur et comprendront : <strong>entrée, plat principal, fromage, dessert, vin et
                            café compris</strong>.
                    </li>
                </ul>

                <p class="important">
                    Prévoir un minimum de 1H15 pour l’installation et la balance
                    Espace scénique 35 m2 : ouverture 7m / profondeur 5m
                    Salle ou Plein air
                </p>

                <h5>Technique</h5>
                <h6>Prise de son</h6>

                <ul>
                    <li>
                        Système de diffusion professionnel (L.ACOUSTICS / ADAMSON / MEYER / NEXO) avec subs
                        <strong>Le système devra couvrir l’espace à sonoriser de manière homogène</strong>
                    </li>
                    <li>
                        6 retours sur 6 circuits égalisés (bss/dbx...)
                    </li>
                    <li>
                        Console analogique (souhaitée) ou numérique 24 in 8 aux minimum, 4 voies de
                        compression, EQ 2x31 bandes sur façade et sur chaque circuit retour, 2 réverbs
                    </li>
                    <li>
                       <strong>
                           Prévoir : 1 chaise sans accoudoir (couleur sombre de préférence) pour le guitariste
                           au lointain-cour + 3 cubes ou petits flycases pour surélever l’ampli basse et les 2 ampli
                           guitare
                       </strong>
                    </li>
                </ul>

                <p style="color: red;">
                    Attention, si déplacements en avion ou train, les artistes ne prendront pas leur backline
                    personnel donc merci de vous munir du matériel suivant :
                </p>
                <ul style="color: red;">
                    <li>
                        1 batterie jazz,
                    </li>
                    <li>
                        1 ampli guitare à lampe type fender blues junior,
                    </li>
                    <li>
                        1 ampli guitare type AER,
                    </li>
                    <li>
                        1 ampli bass type markbass.
                    </li>
                </ul>

                <h4 class="text-center">
                    Plan de scène
                </h4>

                <div class="text-center mb-4">
                    <img src="img/pro/plan-de-scene.png" alt="plan-de-scene"/>
                </div>

                <h4 class="text-center">
                    PATCH LIST
                </h4>

                <div class="table-responsive">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th scope="col">PATCH</th>
                                <th scope="col">INSTRUMENT</th>
                                <th scope="col">PRISE DE SON</th>
                                <th scope="col">Pied</th>
                                <th scope="col">Remarques</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row"></th>
                                <td>KICK</td>
                                <td>B 52 / D112</td>
                                <td>Petit</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>SNARE</td>
                                <td>SM 57</td>
                                <td>Petit</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>HH</td>
                                <td>KM 184 / C 541</td>
                                <td>Petit</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>Tom</td>
                                <td>MD 421 / E 604</td>
                                <td>(Petit)</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>OH Left</td>
                                <td>C 414 / KM 184</td>
                                <td>Petit</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>OH Right</td>
                                <td>C 414 / KM 184</td>
                                <td>Grand</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>CONTREBASSE DI</td>
                                <td>DI BSS / KLARK</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>CONTRABASSE MICRO</td>
                                <td>DPA (fourni)</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>GUITARE ACOUSTIQUE DI</td>
                                <td></td>
                                <td></td>
                                <td>DIRECT OUT AMPLI</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>GUITARE ACOUSTIQUE MICRO</td>
                                <td>KM 184 / C 535</td>
                                <td>Petit</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>GUITARE ELECTRIQUE</td>
                                <td>SM 57 / E 906</td>
                                <td>(Petit)</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>SAXOPHONE</td>
                                <td>B 98 HF (fourni)</td>
                                <td></td>
                                <td>SPARE : MD 421 / SM 57</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>TROMPETTE</td>
                                <td>B 98 HF (fourni)</td>
                                <td>Grand</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>Micro PRESENTATION</td>
                                <td>SM 58</td>
                                <td>Grand</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="downloadBtn">
                <a class="btn musica-btn mb-2" href="datas/pro/fiche-technique.pdf" download="fiche-technique-Hot-Swing-Sextet.pdf">
                    <?= $LANG->L('download_technical_file'), PHP_EOL;?>
                </a>
            </div>

        </div>
    </section>
    <div class="bg-gradients-inverse"></div>

    <!-- @@include cta-section.inc.html {"css":"pro"} -->
<!-- @@close -->
