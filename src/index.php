<!-- @@master = master.php -->

<!-- @@block = content -->
    <section class="home-area">

        <div class="home-slides owl-carousel">

            <div class="single-home-slide dark-filter">
                <div class="slide-img" style="background-image: url(img/bg/porto-swing-tam-scene.jpg);"></div>
                <div class="home-slides-content">
                    <h2 data-animation="fadeInDown" data-delay="300ms">
                        Hot Swing Sextet
                        <span>Hot Swing Sextet</span>
                    </h2>
                </div>
            </div>

            <div class="single-home-slide no-js-hide">
                <div class="slide-img" style="background-image: url(img/bg/swing-shout-scene.jpg);"></div>
                <div class="home-slides-content">
                    <h2 data-animation="fadeInRight" data-delay="300ms">
                        Hot Swing Sextet
                        <span>Hot Swing Sextet</span>
                    </h2>
                </div>
            </div>

            <div class="single-home-slide no-js-hide">
                <div class="slide-img" style="background-image: url(img/bg/F415.jpg);"></div>
                <div class="home-slides-content">
                    <h2 data-animation="fadeInUp" data-delay="300ms">
                        Hot Swing Sextet
                        <span>Hot Swing Sextet</span>
                    </h2>
                </div>
            </div>

        </div>
        <div class="bg-gradients"></div>

        <div class="scrollDown">
            <a href="#new-album">
                Next
                <div class="line"></div>
            </a>
        </div>
    </section>

    <section id="new-album" class="bg-overlay">
        <div class="container">
            <img id="bandeau-whats-your-jive" src="img/album/bandeau-whats-your-jive.jpg" alt="" />

            <div class="videoWrapper">
            <iframe width="100%" class="video" src="https://www.youtube.com/embed/iVFM4L3OIUQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </section>
    <div class="bg-gradients-inverse"></div>

    <section id="about-us" class="bg-overlay">

        <div class="container">

            <div class="main-visuel wow">
                <img src="img/pro/visuel-sans-titre.jpg" alt=""/>
            </div>

            <div class="biography-area">

                <div class="content">
                    <p class="wow">
                        <?= $LANG->L('biography'), PHP_EOL; ?>
                    </p>
                    <div class="signature wow" data-wow-duration="3s">
                        HotSwingSextet
                    </div>
                </div>

                <div class="thumbnails wow">
                    <img src="img/bg/Zeno-F-Pensky__MG_1276.jpg" alt=""/>
                    <img src="img/bg/Zeno-F-Pensky__MG_1265.jpg" class="hidden" alt=""/>
                </div>

            </div>


            <div class="about-musiciens owl-carousel">

                <div class="artiste-card">
                    <h5 class="name">
                        Thibaud Bonté
                    </h5>
                    <img class="wow" src="img/musiciens/Thibaud.jpg" alt="Thibaud Bonté"/>
                    <h6>
                        <?= $LANG->L('trumpeter'), PHP_EOL; ?>
                    </h6>
                </div>

                <div class="artiste-card">
                    <h5 class="name">
                        Bertrand Tessier
                    </h5>
                    <img class="wow" src="img/musiciens/Bertrand.jpg" alt="Bertrand Tessier"/>
                    <h6>
                        <?= $LANG->L('saxophonist'), PHP_EOL; ?>
                    </h6>
                </div>

                <div class="artiste-card">
                    <h5 class="name">
                        Erwann Muller
                    </h5>
                    <img class="wow" src="img/musiciens/Erwann.jpg" alt="Erwann Muller"/>
                    <h6>
                        <?= $LANG->L('guitarist'), PHP_EOL; ?>
                    </h6>
                </div>

                <div class="artiste-card">
                    <h5 class="name">
                        Ludovic Langlade
                    </h5>
                    <img class="wow" src="img/musiciens/Ludovic.jpg" alt="Ludovic Langlade"/>
                    <h6>
                        <?= $LANG->L('guitarist'), PHP_EOL; ?>
                    </h6>
                </div>

                <div class="artiste-card">
                    <h5 class="name">
                        Franck Richard
                    </h5>
                    <img class="wow" src="img/musiciens/Franck.jpg" alt="Franck Richard"/>
                    <h6>
                        <?= $LANG->L('bassist'), PHP_EOL; ?>
                    </h6>
                </div>

                <div class="artiste-card">
                    <h5 class="name">
                        Jéricho Ballan
                    </h5>
                    <img class="wow" src="img/musiciens/Jericho.jpg" alt="Jéricho Ballan"/>
                    <h6>
                        <?= $LANG->L('drummer'), PHP_EOL; ?>
                    </h6>
                </div>

            </div>

        </div>

    </section>
    <div class="bg-gradients-inverse"></div>


<?php require PATH_APP.'php/galleries.php'; ?>

    <section class="gallery-area">

        <i class="no-js-gallery-direction fas fa-angle-double-right" aria-hidden="true"></i>

        <div class="posters-top-slider owl-carousel">
<?php foreach ($topGallery as $image): ?>
            <img class="owl-lazy" src="<?= $image->path; ?>" data-src="<?= $image->path; ?>" alt="<?= $image->title; ?>"/>
<?php endforeach; ?>
        </div>

        <i class="no-js-gallery-direction fas fa-angle-double-right" aria-hidden="true"></i>

    </section>
    <div class="bg-gradients"></div>

    <!-- Concerts section -->
    <section id="concerts">

        <div class="container">

            <div class="section-heading">
                <h4 class="wow">On Tour</h4>
                <h6 class="wow"><?= $LANG->L('comming_next'); ?></h6>
            </div>

<?php $limit = 4; $past = false; require PATH_APP.'php/View/Events.php'; ?>

        </div>

        <div class="bottom-link">
            <a href="concerts-tour.php<?= $LANG->getUrlParam(); ?>">
                <i class="fas fa-music" aria-hidden="true"></i>
                <?= $LANG->L('all_concerts'), PHP_EOL; ?>
            </a>
        </div>
    </section>
    <div class="bg-gradients"></div>

    <!-- Videos section -->
    <section id="videos-section" class="bg-overlay">

        <div class="section-heading">
            <h4 class="wow"><?= $LANG->L('videos'); ?></h4>
        </div>

        <i class="no-js-gallery-direction fas fa-angle-double-right" aria-hidden="true"></i>

        <div class="videos-slides owl-carousel wow">
<?php require PATH_APP.'php/View/Videos.php'; ?>
        </div>

        <i class="no-js-gallery-direction fas fa-angle-double-right" aria-hidden="true"></i>

    </section>
    <div class="bg-gradients-inverse"></div>

    <!-- Featured album section -->
    <section id="featured-album-section">

        <div class="container">

            <blockquote class="quote">
                <strong class="big">“</strong>
                <?= $LANG->L('playlist_citation'), PHP_EOL; ?>
                <strong class="big">”</strong><br/>
                <strong>Action Jazz</strong>
            </blockquote>

            <div class="album-content overflowx-hidden">

                <div class="album-cover wow fadeInLeft">
                    <img src="img/album/cover-whats-your-jive.jpg" alt=""/>
                    <div class="album-buy-area">
                        <div class="album-buy-now">
                            <a href="https://open.spotify.com/album/0mYC5A32mK4xrfGXo0IyBa" class="btn musica-btn" title="Spotify">
                                <?= $LANG->L('buy_on'); ?> <i class="fab fa-spotify" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="album-buy-now">
                            <a href="https://music.apple.com/fr/album/whats-your-jive/1542018686?uo=4&at=1l3v9Tx&ls=1&app=music" class="btn musica-btn" title="Apple">
                                <?= $LANG->L('buy_on'); ?> <i class="fab fa-apple" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="album-overview">

                    <div class="album-info wow">
                        <div class="album-title">
                            <h6>2020</h6>
                            <h5>What's your jive ?</h5>
                        </div>
                    </div>

                    <div id="main-playlister" class="playlistEr wow">
                        <ul></ul>
                        <div class="player wow">
                            <div class="current-song-infos">
                                <span class="track"></span>
                                <span class="title"></span>
                                <span class="duration"></span>
                            </div>
                            <audio preload="none" controls>
                                Your browser does not support HTML5 audio 😢
                            </audio>
                            <button class="btnPrev btn musica-btn">
                                <i class="fas fa-caret-left" aria-hidden="true"></i>
                            </button>
                            <button class="btnPlay btn musica-btn">
                                <i class="fas fa-play" aria-hidden="true"></i>
                            </button>
                            <button class="btnNext btn musica-btn">
                                <i class="fas fa-caret-right" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <div class="bg-gradients"></div>

    <section class="gallery-area">

        <i class="no-js-gallery-direction fas fa-angle-double-right" aria-hidden="true"></i>

        <div class="posters-bottom-slider owl-carousel owl-ltr">
<?php foreach ($bottomGallery as $image): ?>
            <img class="owl-lazy" src="<?= $image->path; ?>" data-src="<?= $image->path; ?>" alt="<?= $image->fileName; ?>"/>
<?php endforeach; ?>
        </div>

        <i class="no-js-gallery-direction fas fa-angle-double-right" aria-hidden="true"></i>

    </section>
    <div class="bg-gradients-inverse"></div>

    <!-- @@include cta-section.inc.html {"css":"index"} -->
<!-- @@close -->

<!-- @@block = scripts -->
    <script src="js/playlistEr.min.js"></script>
    <script src="js/whats-your-jive-playlist.min.js"></script>
<!-- @@close -->
