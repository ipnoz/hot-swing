<!-- @@master = master.php -->

<!-- @@block = content -->
    <div class="breadcumb-area"></div>

    <div class="bg-gradients"></div>

    <section id="mentions-legales-section">

        <div class="container">

            <h4>
                Mentions légales
            </h4>

            <h5>
                1. PRESENTATION
            </h5>

            <ul>
                <li>
                    <h6>Editeur</h6>
                </li>
                <li>
                    Mélodinote
                </li>
                <li>
                    39 bis, rue de Labrède
                </li>
                <li>
                    33800 BORDEAUX
                </li>
            </ul>

            <ul>
                <li>
                    <h6>Directeur de la publication</h6>
                </li>
                <li>
                    Franck Richard
                </li>
            </ul>

            <ul>
                <li>
                    <h6>Responsable de la rédaction</h6>
                </li>
                <li>
                    Franck Richard
                </li>
            </ul>

            <ul>
                <li>
                    <h6>Conception – Réalisation</h6>
                </li>
                <li>
                    © <a href="https://www.ipnoz.net">David Dadon</a>
                </li>
            </ul>

            <ul>
                <li>
                    <h6>Hébergement</h6>
                </li>
                <li>
                    Le site www.hotswingsextet.com est hébergé par:
                </li>
                <li>
                    OVH
                </li>
                <li>
                    2, rue Kellerman
                </li>
                <li>
                    59100 ROUBAIX
                </li>
            </ul>

            <h5>
                2. Données personnelles – Confidentialité
            </h5>

            <p>
                L`association Mélodinote s’engage à prendre toutes précautions utiles pour préserver la sécurité des
                données, et notamment pour éviter qu’elles ne soient déformées, endommagées ou que des tiers non
                autorisés y aient accès. Ces données sont destinées à l`association Mélodinote dans un but informatif
                et ne seront pas transmises à des tiers.
            </p>

            <h5>
                3. Propriété intellectuelle
            </h5>

            <p>
                Les éléments figurant au sein du présent site, tels que sons, images, photographies, vidéos, écrits,
                animations, programmes, charte graphique, utilitaires, bases de données, logiciel, sont protégés par
                les dispositions du Code de la propriété intellectuelle et appartiennent à l’association Mélodinote.
                L’utilisateur s’interdit de porter atteinte aux droits de propriété intellectuelle afférents à ces
                éléments et notamment de les reproduire, représenter, modifier, adapter, traduire, d’en extraire et/ou
                réutiliser une partie qualitativement ou quantitativement substantielle, à l’exclusion des actes
                nécessaires à leur usage normal et conforme.
            </p>

        </div>

    </section>
    <!-- @@include cta-section.inc.html {"css":"mentions-legales"} -->
<!-- @@close -->
