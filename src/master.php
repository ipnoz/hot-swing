<?php require_once 'php/App.php'; ?>
<!DOCTYPE html>
<html class="no-js" lang="<?= $LANG->getCurrent(); ?>" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <title>
        Hot Swing Sextet
    </title>

    <meta name="description" content="Swing Band"/>
    <meta name="keywords"
          content="festival,spectacle,spectacles,show,shows,concert,music,musique,jazz,jazz session,djazz,djazz session,
            danse,danses,lindy hop,lindy,lindi,lyndi,hop,charleston,balboa,balswing,bal,swing,blues,hip hop,studio hop,
            studio,l'Ecole des danses swing,baby,baby party,tea,tea party,party,mistinguettes,sylvia sykes,nick williams,
            david rehm,val salstrom,peter loggins,mia goldsmith,bobby white,kate hedin,laura keat,jeremy otth,
            electro swing"/>
    <meta name="author" content="Colorlib, David Dadon - https://www.ipnoz.net"/>
    <meta name="copyright" content="https://www.hotswingsextet.com/"/>
    <meta name="Language" content="<?= $LANG->getCurrent(); ?>"/>

    <meta name="apple-mobile-web-app-title" content="Hot Swing Sextet"/>
    <meta name="application-name" content="Hot Swing Sextet"/>
    <meta name="msapplication-TileColor" content="#da532c"/>
    <meta name="theme-color" content="#ffffff"/>

    <meta property="og:title" content="Hot Swing Sextet"/>
    <meta property="og:site_name" content="Hot Swing Sextet"/>
    <meta property="og:description" content="Swing Band"/>
    <meta property="og:url" content="https://www.hotswingsextet.com/"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="http://hotswingsextet.com/img/pro/visuel.jpeg"/>
    <meta property="og:image:width" content="550"/>
    <meta property="og:image:height" content="363"/>
    <meta property="og:locale" content="<?= $LANG->getCurrent(); ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>

    <link rel="apple-touch-icon" sizes="96x96" href="img/logo/logo-96.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="img/logo/logo-hot-32.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="img/logo/logo-hot-16.png"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allura:200,300,400,500,600,700,800,900"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700,800,900"/>
    <link rel="stylesheet" href="vendor/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="vendor/css/fontawesome-all.min.css"/>
    <link rel="stylesheet" href="vendor/css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="vendor/css/owl.theme.default.min.css"/>
    <link rel="stylesheet" href="vendor/css/animate.min.css"/>
    <link rel="stylesheet" href="vendor/css/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="css/playlistEr.min.css"/>
    <link rel="stylesheet" href="css/style.min.css"/>

    <script src="vendor/js/HTML5-boilerplate.min.js"></script>
    <script src="vendor/js/jquery.min.js"></script>
    <script>
        $('html').removeClass('no-js').addClass('js');
    </script>
</head>

<body>

    <div id="preloader">
        <div class="animation">
            <picture>
                <source srcset="img/logo/logo-128.png" media="(min-width: 991px)"/>
                <img src="img/logo/logo-96.png" alt=""/>
            </picture>
        </div>
    </div>
    <script src="js/preloader.min.js"></script>

    <header id="header">

        <img class="logo logo-side" src="img/logo/logo-96.png" alt="Logo-96">

        <button type="button" aria-controls="menu" aria-expanded="false" aria-label="Toggle menu">
            <span></span>
            <span></span>
            <span></span>
        </button>

        <div id="menu">
            <nav>
                <a class="logo logo-center" href="./<?= $LANG->getUrlParam(); ?>">
                    <img src="img/logo/logo-96.png" alt="Logo-96">
                </a>
                <ul>
                    <li>
                        <a href="./<?= $LANG->getUrlParam(); ?>">Home</a>
                    </li>
                    <li>
                        <a href="concerts-tour.php<?= $LANG->getUrlParam(); ?>">Concerts</a>
                    </li>
                    <li>
                        <a href="albums.php<?= $LANG->getUrlParam(); ?>">Albums</a>
                    </li>
                    <li>
                        <a href="pro.php<?= $LANG->getUrlParam(); ?>">Pro</a>
                    </li>
                    <li>
                        <a href="contact.php<?= $LANG->getUrlParam(); ?>">Contact</a>
                    </li>
                </ul>

                <div class="top-social-info">
                    <a href="https://www.instagram.com/hotswingsextet/" title="<?= $LANG->L('menu_insta'); ?>">
                        <i class="fab fa-instagram" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.facebook.com/hotswingsextet/" title="<?= $LANG->L('menu_fb'); ?>">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                    </a>
                    <a href="https://soundcloud.com/hot-swing-sextet" title="<?= $LANG->L('menu_snd'); ?>">
                        <i class="fab fa-soundcloud" aria-hidden="true"></i>
                    </a>
                    <a href="https://itunes.apple.com/fr/artist/hot-swing-sextet/1041191255" title="<?= $LANG->L('menu_itu'); ?>">
                        <i class="fab fa-apple" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UC52QuaQy7bp7fbilE_SwY0g" title="<?= $LANG->L('menu_yt'); ?>">
                        <i class="fab fa-youtube" aria-hidden="true"></i>
                    </a>
                    <a href="https://melodinoteproduction.bandcamp.com/album/hot-swing-sextet" title="<?= $LANG->L('menu_bp'); ?>">
                        <i class="fab fa-bandcamp" aria-hidden="true"></i>
                    </a>
                    <a href="?lang=<?= $LANG->getMenuParam(); ?>"><?= $LANG->getMenuText(); ?></a>
                </div>
            </nav>
        </div>
    </header>

    <!-- @@placeholder = content -->

    <footer id="footer">

        <div class="mentions">
            <a href="mentions-legales.php<?= $LANG->getUrlParam(); ?>">Mentions légales (french)</a>
        </div>

        <div class="widgets">

            <div class="signature">
                <div class="wow lightSpeedIn">HotSwingSextet</div>
            </div>

            <div class="follow-us">
                <div class="title">
                    <h5><?= $LANG->L('follow_us'); ?></h5>
                </div>
                <ul class="social key-notes">
                    <li>
                        <a href="https://www.instagram.com/hotswingsextet/">
                            <i class="fab fa-instagram" aria-hidden="true"></i> Instagram
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/hotswingsextet/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i> Facebook
                        </a>
                    </li>
                    <li>
                        <a href="https://soundcloud.com/hot-swing-sextet">
                            <i class="fab fa-soundcloud" aria-hidden="true"></i> Soundcloud
                        </a>
                    </li>
                    <li>
                        <a href="https://itunes.apple.com/fr/artist/hot-swing-sextet/1041191255">
                            <i class="fab fa-apple" aria-hidden="true"></i> Itunes
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UC52QuaQy7bp7fbilE_SwY0g">
                            <i class="fab fa-youtube" aria-hidden="true"></i> Youtube
                        </a>
                    </li>
                    <li>
                        <a href="https://melodinoteproduction.bandcamp.com/album/hot-swing-sextet">
                            <i class="fab fa-bandcamp" aria-hidden="true"></i> Bandcamp
                        </a>
                    </li>
                </ul>
            </div>

            <div class="contact-us">
                <div class="title">
                    <h5><?= $LANG->L('contact_us'); ?></h5>
                </div>
                <ul>
                    <li><h6><?= $LANG->L('scene_tour_concert'); ?></h6></li>
                    <li>Rudy Lannou</li>
                    <li>+33 6 64 20 18 22</li>
                    <li><a href="mailto:rudy@melodinote.fr">rudy@melodinote.fr</a></li>
                    <li>Bordeaux, FRANCE</li>
                    <li>
                        <a href="http://www.melodinote.fr/">
                            <img src="img/vendor/melodinote/logo-noir-64x64.png" alt=""/>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li><h6><?= $LANG->L('technical_control'); ?></h6></li>
                    <li>Franck Richard</li>
                    <li>+33 6 65 26 69 87</li>
                    <li><a href="mailto:frich33@gmail.com">frich33@gmail.com</a></li>
                    <li>Bordeaux, FRANCE</li>
                </ul>
            </div>

        </div>

        <div class="copywrites">
            <div class="authors">
                <div>
                    <?= $LANG->L('developed_by'); ?> <a href="https://www.ipnoz.net">David Dadon</a>
                </div>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                <div>
                    Template made with <i class="far fa-heart" aria-hidden="true"></i> by
                    <a href="https://colorlib.com" target="_blank">Colorlib</a>
                </div>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </div>

            <div class="owner">
                &copy;
                <script>document.write(new Date().getFullYear());</script>
                HotSwingSextet | All rights reserved
            </div>
        </div>

    </footer>

    <script src="vendor/js/popper.min.js"></script>
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/owl.carousel.min.js"></script>
    <script src="vendor/js/jquery.fancybox.min.js"></script>
    <script src="vendor/js/jquery.scrollUp.min.js"></script>
    <script src="vendor/js/jquery.easing.min.js"></script>
    <script src="vendor/js/wow.min.js"></script>
    <!-- @@placeholder = scripts -->
    <script src="js/main.min.js"></script>
</body>

</html>
